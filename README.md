
# Hello!

> if you have some problems when you build this project via "mvn clean install"
> you have to connect to the database "H2"
> because it is used in the "homework_05_JDBS_SQL_Liquibase" or delete that module
> Now when the project is rebuilding you need to use the button "Skip Tests"
> in the "Maven" tab! This is due to the fact that in the
> "homework_12_HTTP_HTTPS protocols" for tests we have to start the container
> of servlets "Tomcat" because we use the "URL" for running the tests. It is only
> after running the "Tomcat" you can turn off the button "Skip Tests"
> the "Maven" tab and rebuild project via "mvn clean install"
> or "Lifecycle" -> clean -> install -> "Run Maven Build" (using "test").

* 1) homework_01_Git: branches - master, master_homework_01, homework_01, homework_01_b
* 2) homework_02_Maven_Logging: branch - homework_02_Maven_Logging
>       read the instruction in the homework_02_Maven_Logging/README.md
* 3) homework_03_ArrayList_LinkedList: branch - homework_03_ArrayList_LinkedList
* 4) homework_04_Collections_StreamAPI: branch - homework_04_Collections_StreamAPI
* 5) homework_05_JDBS_SQL_Liquibase: branch - homework_05_JDBS_SQL_Liquibase
>       read the instuction in the homework_05_JDBS_SQL_Liquibase/README.md
* 6) homework_06_JUnit_Mockito: branch - homework_06_JUnit_Mockito
* 7) homework_07_Java_IO_NIO2: branch - homework_07_Java_IO_NIO2
* 8) homework_08_Concurrent_Multithreading:
*       branch - homework_08_Concurrent_Multithreading
* 9) homework_09_Design_Patterns_in_Java:
*       branch - homework_09_Design_Patterns_in_Java
>       read the description of the design patterns in the
>       homework_09_Design_Patterns_in_Java/README.md
* 10) homework_10_Regular_expressions_Serialization:
*       branch - homework_10_Regular_expressions_Serialization
* 11) homework_11_GitLab_CI: branch - homework_11_GitLab_CI
>       read the instruction in the homework_11_GitLab_CI/README.md
* 12) homework_12_HTTP_HTTPS_protocols: branch - homework_12_HTTP_HTTPS_protocols
>       read the instruction in the bomework_12_HTTP_HTTPS_protocols/README.md
* 13) homework_13_REST: branch - homework_13_REST
>       read the instruction in the homework_13_REST/README.md
* 14) homework_14_Servlets_JSP_JSTL: branch - homework_14_Servlets_JSP_JSTL
* 15) homework_15_Sessions_Cookies: branch - homework_15_Sessions_Cookies
* 16) homework_16_Localization: branch - homework_16_Localization
* 17) pet_project_CarsOrders: branch - pet_project_CarsOrders