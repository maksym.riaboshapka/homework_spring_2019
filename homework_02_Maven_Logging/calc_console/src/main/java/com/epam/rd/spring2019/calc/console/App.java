package com.epam.rd.spring2019.calc.console;

import com.epam.rd.spring2019.calc.core.CalcImpl;
import com.epam.rd.spring2019.calc.interfaces.Calc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

    private static final  Logger LOGGER = LoggerFactory.getLogger(App.class.getSimpleName());
    private static final String NUMBER_1 = "number1 = ";
    private static final String NUMBER_2 = ", number2 = ";
    private static final String OPERATOR = ", operator = ";
    private static final String RESULT = ", result = ";

    public static void main(String[] args) {
        if (args.length == 3) {
            LOGGER.info("Entering application.");
            LOGGER.debug("Debug from App");
            Calc calc = new CalcImpl();

            double a = getArgsElement(args, 0);

            double b = getArgsElement(args, 1);

            String mathOperator = args[2];

            switch (mathOperator) {
                case "+":
                    System.out.println(NUMBER_1 + a + NUMBER_2 + b
                            + OPERATOR + mathOperator + RESULT + calc.addition(a, b));
                    break;
                case "-":
                    System.out.println(NUMBER_1 + a + NUMBER_2 + b
                            + OPERATOR + mathOperator + RESULT + calc.subtraction(a, b));
                    break;
                case "*":
                    System.out.println(NUMBER_1 + a + NUMBER_2 + b
                            + OPERATOR + mathOperator + RESULT + calc.multiplication(a, b));
                    break;
                case "/":
                    System.out.println(NUMBER_1 + a + NUMBER_2 + b
                            + OPERATOR + mathOperator + RESULT + calc.division(a, b));
                    break;
                default:
                    System.out.println("An incorrect maths operation. Try again, please!");

            }
            LOGGER.info("Exiting application.");

        } else if (args.length == 0) {
            System.err.println("Invalid input data: String[] args is empty!");
            LOGGER.debug("Invalid input data: String[] args is empty!");
        } else if (args.length == 2) {
            System.err.println("Please enter a number or an arithmetic operator!");
            LOGGER.debug("Please enter a number or an arithmetic operator!");
        } else {
            System.err.println("Invalid input data!");
            LOGGER.debug("Invalid input data!");
        }
    }

    private static double getArgsElement(String[] args, int i) {
        String argsElement = args[i];
        return Double.parseDouble(argsElement);
    }


}
