

# homework_02_Maven_Logging
# maven-multimodule-example

## This program prints Command-Line Arguments

* To build project execute in cmd: 
> mvn clean install

* To run application execute in cmd:

// success scenario
> java -jar .\homework_02_Maven_Logging\calc_console\target\calc-jar-with-dependencies.jar 100 -100 +

> java -jar .\homework_02_Maven_Logging\calc_console\target\calc-jar-with-dependencies.jar 100 -100 -

> java -jar .\homework_02_Maven_Logging\calc_console\target\calc-jar-with-dependencies.jar 7 3 "*"

> java -jar .\homework_02_Maven_Logging\calc_console\target\calc-jar-with-dependencies.jar 56 7 /

// fail scenario
>  java -jar .\homework_02_Maven_Logging\calc_console\target\calc-jar-with-dependencies.jar

>  java -jar .\homework_02_Maven_Logging\calc_console\target\calc-jar-with-dependencies.jar 100 -100

>  java -jar .\homework_02_Maven_Logging\calc_console\target\calc-jar-with-dependencies.jar 100