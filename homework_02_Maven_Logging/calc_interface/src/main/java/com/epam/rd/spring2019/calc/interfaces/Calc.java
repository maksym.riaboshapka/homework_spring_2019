package com.epam.rd.spring2019.calc.interfaces;

public interface Calc {

    /** an adding operation of two parameters
     *
     * @param a for the adding operation
     * @param b for the adding operation
     * @return the result of the adding operation
     */
    double addition(double a, double b);

    /**a subtraction operation of two parameters
     *
     * @param a for the subtraction operation
     * @param b for the subtraction operation
     * @return the result of the subtraction operation
     */
    double subtraction(double a, double b);

    /**a multiplication operation of two parameters
     *
     * @param a for the multiplication operation
     * @param b for the multiplication operation
     * @return the result of the multiplication operation
     */
    double multiplication(double a, double b);

    /**a division operation of two parameters
     *
     * @param a for the division operation
     * @param b for the division operation
     * @return the result of the division operation
     */
    double division(double a, double b);

}