
For correct operation of the GitLab_CI is necessary that .gitlab-ci.yml-file located
in the root project folder and the repository. GitLab CI/CD uses a file in the root
of the repo, named .gitlab-ci.yml, to read the definitions for jobs that will be
executed by the configured GitLab Runners.
To demonstrate my work with GitLab_CI, I also created a separate folder and repository.

https://gitlab.com/maksym.riaboshapka/homework_11_gitlab_ci/pipelines

My binary artifacts:
https://bintray.com/makmr11/my-labs - bintray.com

