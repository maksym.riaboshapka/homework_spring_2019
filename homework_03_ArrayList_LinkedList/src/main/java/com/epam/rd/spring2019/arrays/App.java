package com.epam.rd.spring2019.arrays;

import com.epam.rd.spring2019.arrays.console.MyArrayListConsole;
import com.epam.rd.spring2019.arrays.console.MyLinkedListConsole;

public class App {

    public static void main(String[] args) {

        MyArrayListConsole.workWithMyArrayList();
        MyLinkedListConsole.workWithMyLinkedList();

    }

}
