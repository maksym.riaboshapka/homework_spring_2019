package com.epam.rd.spring2019.arrays.core.impl;

import com.epam.rd.spring2019.arrays.core.MyArryaList;

public class MyArrayListImpl<E> implements MyArryaList<E> {

    public static final int DEFAULT_CAPACITY = 10;
    private int arrayCount;
    private E[] elementData;
    private E[] emptyElementdata = (E[]) new Object[]{};

    public MyArrayListImpl() {
        elementData = (E[]) new Object[DEFAULT_CAPACITY];
    }

    public MyArrayListImpl(int initialCapacity) {
        if (initialCapacity > 0) {
            elementData = (E[]) new Object[initialCapacity];
        } else if (initialCapacity == 0) {
            elementData = emptyElementdata;
        } else {
            try {
                throw new IllegalArgumentException("\nIllegal Capacity: " +
                        initialCapacity);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public E get(int index) {
        if (myCheckArrayIndex(index)) {
            return elementData[index];
        } else {
            return null;
        }
    }

    @Override
    public int size() {
        return elementData.length;
    }

    @Override
    public boolean add(E element) {
        add(element, arrayCount);
        arrayCount++;
        return true;
    }

    @Override
    public boolean add(int index, E element) {
        if (myCheckArrayIndex(index)) {
            E[] tempArray = (E[]) new Object[elementData.length + 1];
            for (int i = 0; i < elementData.length; i++) {
                if (i == index) {
                    E tempElement = elementData[i];
                    tempArray[i] = element;
                    tempArray[i + 1] = tempElement;
                    int tempArrayIndexForInnerCycle = i + 1;
                    for (int j = i + 2; j < tempArray.length; j++) {
                        tempArray[j] = elementData[tempArrayIndexForInnerCycle++];
                    }
                    elementData = tempArray;
                    return true;
                } else {
                    tempArray[i] = elementData[i];
                }
            }
        }
        return false;
    }

    @Override
    public E set(int index, E element) {
        if (myCheckArrayIndex(index)) {
            E tempElement = elementData[index];
            elementData[index] = element;
            return tempElement;
        }
        return null;
    }

    @Override
    public E remove(int index) {
        if (myCheckArrayIndex(index)) {
            E[] tempArray = (E[]) new Object[elementData.length - 1];
            for (int i = 0; i < tempArray.length; i++) {
                if (i == index) {
                    E tempElement = elementData[i];
                    tempArray[i] = elementData[i + 1];
                    int tempArrayIndexForInnerCycle = i + 2;
                    for (int j = i + 1; j < tempArray.length; j++) {
                        tempArray[j] = elementData[tempArrayIndexForInnerCycle++];
                    }
                    elementData = tempArray;
                    return tempElement;
                } else {
                    tempArray[i] = elementData[i];
                }
            }
        }
        return null;
    }

    private void add(E elementForAdding, int arrayCount) {
        if (arrayCount == elementData.length) {
            E[] tempNewArray = createNewArray(elementData);
            elementData = tempNewArray;
            elementData[arrayCount] = elementForAdding;
            return;
        }
        elementData[arrayCount] = elementForAdding;
    }

    private E[] createNewArray(Object[] elementData) {
        int sizeOfOldArray = size();
        int sizeOfNewArray = ((sizeOfOldArray * 3) / 2) + 1;
        E[] tempNewArray = (E[]) new Object[sizeOfNewArray];
        for (int i = 0; i < elementData.length; i++) {
            E tempElement = (E) elementData[i];
            tempNewArray[i] = tempElement;
        }
        return tempNewArray;
    }

    private boolean myCheckArrayIndex(int index) {
        if (index < 0 || index >= elementData.length) {
            try {
                throw new ArrayIndexOutOfBoundsException("Check the" +
                        " introduced index because the current index" +
                        " \"" + index + "\" is illegal!");
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            } finally {
                return false;
            }
        }
        return true;
    }
}