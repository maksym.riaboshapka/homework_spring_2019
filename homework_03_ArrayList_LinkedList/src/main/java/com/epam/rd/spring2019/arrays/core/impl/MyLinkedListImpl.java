package com.epam.rd.spring2019.arrays.core.impl;

import com.epam.rd.spring2019.arrays.core.MyLinkedList;

public class MyLinkedListImpl<E> implements MyLinkedList<E> {
    private int sizeList;
    private Link first;
    private Link last;

    public MyLinkedListImpl() {
        first = null;
        last = null;
    }

    public MyLinkedListImpl(int size) {
        for (int i = 0; i < size; i++) {
            addLast(null);
        }
    }

    private class Link {
        private E obj;
        private Link nextElement;
        private Link previousElement;

        private Link(E ob) {
            obj = ob;
        }

        private void displayLink() {             // вивід вмісту елемента
            System.out.print(obj + " ");
        }
    }

    @Override
    public int size() {
        Link current = first;
        sizeList = 0;
        while (current != null) {
            current = current.nextElement;
            sizeList++;
        }
        return sizeList;
    }

    @Override
    public void addFirst(E element) {
        Link newLink = new Link(element);
        if (isEmpty()) {
            last = newLink;
        } else {
            first.previousElement = newLink;
        }
        newLink.nextElement = first;
        first = newLink;
    }

    @Override
    public void addAfter(E index, E element) {
        Link current = first;
        while (current.obj != index) {
            current = current.nextElement;
        }
        Link newLink = new Link(element);
        if (current == last) {
            newLink.nextElement = null;
            last = newLink;
        } else {
            newLink.nextElement = current.nextElement;
            current.nextElement.previousElement = newLink;
        }
        newLink.previousElement = current;
        current.nextElement = newLink;
    }

    @Override
    public void addLast(E element) {
        Link newLink = new Link(element);
        if (isEmpty()) {
            first = newLink;
        } else {
            last.nextElement = newLink;
            newLink.previousElement = last;
        }
        last = newLink;
    }

    @Override
    public void removeFirst() {
        if (first.nextElement == null) {
            last = null;
        } else {
            first.nextElement.previousElement = null;
        }
        first = first.nextElement;
    }

    @Override
    public void remove(E element) {
        Link current = first;

        while (current.obj != element) {
            current = current.nextElement;
        }

        if (current == first) {
            first = current.nextElement;
        } else {
            current.previousElement.nextElement = current.nextElement;
        }

        if (current == last) {
            last = current.previousElement;
        } else {
            current.nextElement.previousElement = current.previousElement;
        }
    }

    @Override
    public void removeLast() {
        if (first.nextElement == null) {
            first = null;
        } else {
            last.previousElement.nextElement = null;
        }
        last = last.previousElement;
    }

    @Override
    public E getFirst() {
        Link current = first;
        if (current == null) {
            return null;
        }
        return current.obj;
    }

    @Override
    public E get(E element) {
        Link current = first;
        while (current.obj != element) {
            current = current.nextElement;
        }
        return current.obj;
    }

    @Override
    public E getLast() {
        Link current = last;
        if (current == null) {
            return null;
        }
        return last.obj;
    }

    @Override
    public void displayList() {
        System.out.print("MyLinkedList from \"firstElement\" to \"lastElement\": ");
        Link current = first;
        while (current != null) {
            current.displayLink();
            current = current.nextElement;
        }
        System.out.println("");
    }

    private boolean isEmpty() {
        return (first == null);
    }
}
