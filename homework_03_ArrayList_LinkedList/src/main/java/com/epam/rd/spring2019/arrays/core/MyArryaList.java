package com.epam.rd.spring2019.arrays.core;

public interface MyArryaList<E> {

    /**
     * Returns the number of elements of the MyArrayList
     *
     * @return the number of elements in this list
     */
    int size();

    /**
     * Appends the specified element to the end of the MyArrayList
     *
     * @param e element to be appended to the MyArrayList
     * @return true if element is appended to the end of the MyArrayList
     */
    boolean add(E e);

    /**
     * Inserts the specified element at the specified position of the MyArrayList
     *
     * @param index   index at which the specified element is to be inserted
     * @param element element to be inserted
     * @return true if element is inserted at the specified position of the MyArrayList
     */
    boolean add(int index, E element);

    /**
     * Returns the element from the specified position of the MyArrayList
     *
     * @param index of the element
     * @return element from the specified position in this list
     */
    E get(int index);

    /**
     * Replaces the element with the specified element at the specified position of the MyArrayList
     *
     * @param index   of the element to replace
     * @param element to be stored at the specified position
     * @return the element previously at the specified position
     */
    E set(int index, E element);

    /**
     * Removes the element at the specified position of the MyArrayList
     *
     * @param index of the element to be removed
     * @return that was removed from the list
     */
    E remove(int index);
}
