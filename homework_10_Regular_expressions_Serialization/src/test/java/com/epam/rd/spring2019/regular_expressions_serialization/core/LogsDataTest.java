package com.epam.rd.spring2019.regular_expressions_serialization.core;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LogsDataTest {

    private static final String DATA_AND_TIME = "2018-01-01";
    private static final String TYPE = "INFO";
    private static final String MODULE = "ASC";
    private static final String OPERATION = "Login";

    @Test
    public void getModule() {
        //GIVEN
        LogsData logsDataTest = new LogsData(DATA_AND_TIME,
                TYPE, MODULE, OPERATION, 100000);
        //WHEN
        logsDataTest.getModule();
        //THEN
        assertEquals(MODULE, "ASC");
    }

    @Test
    public void getExecutionTime() {
        //GIVEN
        LogsData logsDataTest = new LogsData(DATA_AND_TIME,
                TYPE, MODULE, OPERATION, 100000);
        //WHEN
        logsDataTest.getExecutionTime();
        //THEN
        assertEquals(10000, 10000);
    }

    @Test
    public void compareTo() {
        //GIVEN
        LogsData logsDataTest = new LogsData(DATA_AND_TIME,
                TYPE, MODULE, OPERATION, 100000);
        LogsData logsDataTest1 = new LogsData(DATA_AND_TIME,
                TYPE, MODULE, OPERATION, 100000);
        //WHEN
        logsDataTest.compareTo(logsDataTest1);
        //THEN
        assertEquals(0, 0);
    }
}