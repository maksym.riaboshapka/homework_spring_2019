package com.epam.rd.spring2019.regular_expressions_serialization.core;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WorkWithLogsFile {


    private static final String LOGS_FILE_NAME =
            "D:\\EPAM_University_Programs_UA_Spring_2019" +
                    "\\homework_spring_2019" +
                    "\\homework_10_Regular_expressions_Serialization" +
                    "\\src\\main\\resources\\logs.txt";

    private static final String SAVED_RESULTS =
            "D:\\EPAM_University_Programs_UA_Spring_2019" +
                    "\\homework_spring_2019" +
                    "\\homework_10_Regular_expressions_Serialization" +
                    "\\src\\main\\resources\\saved_results.dat";

    private static final String REGEX = "^(\\d{4}-\\d{2}-\\d{2}\\s+\\d{2}:\\d{2}:\\d{2}.\\d{3})\\s+" +
            "(DEBUG|ERROR|FATAL|INFO|TRACE|WARN)\\s+" +
            "(Module=+)" +
            "(ASC|CMN|LAB|LMC|LWS|MIC)\\s+" +
            "(Operation:)\\s+" +
            "(Get Order|Find Product|Logout|Login|Save Order)\\s+" +
            "(Execution time:)\\s+" +
            "(\\d+)\\s+" +
            "(ms)$";

    public void analyzeLogsFile() {
        try {
            WorkWithLogsFile workWithLogsFile = new WorkWithLogsFile();
            int times = 10;
            System.out.print("Top " + times + " operations in " + LOGS_FILE_NAME + " file:\n");
            List<LogsData> listLog = workWithLogsFile
                    .getListLog(Files.lines(Paths.get(LOGS_FILE_NAME))
                            .collect(Collectors.toList()));
            Collections.sort(listLog);
            PrintWriter printWriter = new PrintWriter(new FileWriter(SAVED_RESULTS));
            String module = "";
            for (LogsData rowOut : listLog) {
                if (!module.equals(rowOut.getModule())) {
                    printWriter.println(rowOut.getModule() + " Module:");
                    for (int i = listLog.indexOf(rowOut); i < listLog.indexOf(rowOut) + times; i++) {
                        printWriter.println(listLog.get(i).toString());
                    }
                    module = rowOut.getModule();
                }
            }
            printWriter.close();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    public List<LogsData> getListLog(List<String> list) {
        ArrayList<LogsData> logsData = new ArrayList<>();
        for (String s : list) {
            LogsData readLogs = readLogsFile(s);
            logsData.add(readLogs);
        }
        return logsData;
    }

    public LogsData readLogsFile(String row) {
        Matcher matcher = Pattern.compile(REGEX).matcher(row);
        LogsData logsData = null;
        while (matcher.find()) {
            logsData = new LogsData(matcher.group(1), matcher.group(2),
                    matcher.group(4), matcher.group(6), Integer.valueOf(matcher.group(8)));
        }
        return logsData;
    }


    public void loadSavedResult() {
        try {
            FileReader fileReader = new FileReader(SAVED_RESULTS);
            try (Scanner scan = new Scanner(fileReader)) {
                while (scan.hasNextLine()) {
                    System.out.println(scan.nextLine());
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        }

    }

}
