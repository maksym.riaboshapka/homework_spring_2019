package com.epam.rd.spring2019.regular_expressions_serialization.console;

import com.epam.rd.spring2019.regular_expressions_serialization.core.WorkWithLogsFile;

public class App {

    public static void main(String[] args) {

        WorkWithLogsFile workWithLogsFile = new WorkWithLogsFile();
        workWithLogsFile.analyzeLogsFile();
        workWithLogsFile.loadSavedResult();

    }


}
