package com.epam.rd.spring2019.regular_expressions_serialization.core;

import java.io.Serializable;

public class LogsData implements Serializable, Comparable<LogsData> {

    private String dateAndTime;
    private String type;
    private String module;
    private String operation;
    private Integer executionTime;

    public LogsData(String dateAndTime,
                    String type,
                    String module,
                    String operation,
                    Integer executionTime) {
        this.dateAndTime = dateAndTime;
        this.type = type;
        this.module = module;
        this.operation = operation;
        this.executionTime = executionTime;
    }


    public String getModule() {
        return module;
    }

    public Integer getExecutionTime() {
        return executionTime;
    }


    @Override
    public int compareTo(LogsData logsData) {
        int result = module.compareTo(logsData.getModule());
        if (result == 0) {
            return Integer.valueOf(logsData.getExecutionTime()).compareTo(executionTime);
        } else return result;
    }


    @Override
    public String toString() {

        return "\t" + " Operation: " + operation + " " + " type: " + type + " " +
                executionTime + " ms, " + "finished at " + dateAndTime;
    }

}
