package com.epam.rd.spring2019.junit_mockito.processor;

public class Processor {
    private Producer producer;
    private Consumer consumer;

    public void process() {
        String value = producer.produce();

        if (value == null) {
            throw new IllegalStateException();
        }

        consumer.consume(value);
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public void setConsumer(Consumer consumer) {
        this.consumer = consumer;
    }
}
