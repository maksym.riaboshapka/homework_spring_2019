package com.epam.rd.spring2019.junit_mockito.console;

import com.epam.rd.spring2019.junit_mockito.core.MyArrayList;
import com.epam.rd.spring2019.junit_mockito.core.impl.MyArrayListImpl;

public class MyArrayListConsole {

    public static void workWithMyArrayList() {
        // creating and working with the empty MyArrayList via a default constructor
        MyArrayList<Double> myEmptyDoubleArrayList = new MyArrayListImpl<>();
        int emptyDoubleArrayList = myEmptyDoubleArrayList.size();
        System.out.println("The size of the empty \"emptyDoubleArrayList\" (default)" +
                " is: " + emptyDoubleArrayList);
        for (int i = 0; i < emptyDoubleArrayList; i++) {
            System.out.println("element[" + i + "]: " + myEmptyDoubleArrayList.get(i));
        }

        // creating and working with the MyArrayList
        // with some capacity which is set via the constructor
        MyArrayList<Integer> myIntegerArrayList = new MyArrayListImpl<>(5);
        int capacityMyIntegerArrayList = myIntegerArrayList.size();
        System.out.println("\nThe initial size of the created \"myIntegerArrayList\"" +
                " array (Integer) is: " + capacityMyIntegerArrayList);
        for (int i = 0; i < capacityMyIntegerArrayList; i++) {
            myIntegerArrayList.add(i * 3);
            System.out.println("element[" + i + "]: " + myIntegerArrayList.get(i));
        }
        System.out.println("\nAdding new elements to the existing array" +
                " \"myIntegerArrayList\"");
        for (int i = 0; i < 5; i++) {
            myIntegerArrayList.add((i + 1) * 5);
        }
        for (int i = 0; i < myIntegerArrayList.size(); i++) {
            System.out.println("element[" + i + "]: " + myIntegerArrayList.get(i));
        }
        System.out.println();

        // check the method add(int index, Object element)
        System.out.println("Inserts the specified element at the specified position of the MyArrayList");
        myIntegerArrayList.add(5, 30);
        for (int i = 0; i < myIntegerArrayList.size(); i++) {
            System.out.println("element[" + i + "]: " + myIntegerArrayList.get(i));
        }
        System.out.println();

        // show the work of replacing ot the element with the specified
        // element at the specified position of the MyArrayList
        System.out.println("The current element at the \"5\" position of the \"MyArrayList\" is: " +
                myIntegerArrayList.get(5) +
                "\nReplaces the current element \"" + myIntegerArrayList.set(5, 50) +
                "\" with the specified element \"50\" \"" +
                "\" at the specified position \"5\"." +
                "\nThe current element after replacing is: \"" + myIntegerArrayList.get(5) + "\"\n");

        System.out.println("The state of the \"MyArrayList\" after replacing is: ");
        for (int i = 0; i < myIntegerArrayList.size(); i++) {
            System.out.println("element[" + i + "]: " + myIntegerArrayList.get(i));
        }
        System.out.println();


        System.out.println("Removes the element \"" + myIntegerArrayList.remove(5) + "\" at the specified position \"5\" of the MyArrayList");
        System.out.println("The state of the \"MyArrayList\" after replacing is: ");
        for (int i = 0; i < myIntegerArrayList.size(); i++) {
            System.out.println("element[" + i + "]: " + myIntegerArrayList.get(i));
        }
        System.out.println();

        // attempt to get an array's element via an incorrect array's argument
        System.out.println(myIntegerArrayList.get(-5));
        System.out.println();

        // show the work of the illegal array which is had an illegal array argument
        MyArrayList<String> uncorrectArray = new MyArrayListImpl<>(-5);
        System.out.println();

    }

}
