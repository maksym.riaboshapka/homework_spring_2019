package com.epam.rd.spring2019.junit_mockito.processor;

public class Producer {
    public String produce() {
        return "Magic value";
    }
}
