package com.epam.rd.spring2019.junit_mockito.core;

public interface MyLinkedList<E> {

    /**
     * Returns the number of elements of the MyLinkedList
     *
     * @return the number of elements of the MyLinkedList
     */
    int size();

    /**
     * Inserts the specified element at the beginning of the MyLinkedList
     *
     * @param element the element to add
     */
    void addFirst(E element);

    /**
     * Inserts the specified element at the specified position of the MyLinkedList
     *
     * @param index   at which the specified element is to be inserted
     * @param element the element is inserted
     */
    void addAfter(E index, E element);

    /**
     * Appends the specified element to the end of the MyLinkedList
     *
     * @param element the element is inserted
     */
    void addLast(E element);

    /**
     * Removes the first element's link from the MyLinkedList
     */
    void removeFirst();

    /**
     * Removes the first occurrence of the specified element from the MyLinkedList,
     * if it is present
     *
     * @param element the element is removed from this list, if it is presented
     */
    void remove(E element);

    /**
     * Removes the last element's link from the MyLinkedList
     */
    void removeLast();

    /**
     * Returns the first element in the MyLinkedList
     *
     * @return the first element in the MyLinkedList
     */
    E getFirst();

    /**
     * Returns the specified element of the MyLinkedList
     *
     * @param element element is returned
     * @return the specified element of the MyLinkedList
     */
    E get(E element);

    /**
     * Returns the last element of the MyLinkedList
     *
     * @return the last element of the MyLinkedList
     */
    E getLast();

    /**
     * Outputting the linked list's elements
     */
    void displayList();

}
