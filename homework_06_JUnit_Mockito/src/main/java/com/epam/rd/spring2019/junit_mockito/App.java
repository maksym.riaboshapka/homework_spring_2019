package com.epam.rd.spring2019.junit_mockito;

import com.epam.rd.spring2019.junit_mockito.console.MyArrayListConsole;
import com.epam.rd.spring2019.junit_mockito.console.MyLinkedListConsole;

public class App {

    public static void main(String[] args) {

        MyArrayListConsole.workWithMyArrayList();
        MyLinkedListConsole.workWithMyLinkedList();

    }


}
