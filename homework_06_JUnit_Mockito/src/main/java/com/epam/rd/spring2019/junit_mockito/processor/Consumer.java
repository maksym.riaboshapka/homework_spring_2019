package com.epam.rd.spring2019.junit_mockito.processor;

public class Consumer {
    public void consume(String value) {
        System.out.println("Consumed -> " + value);
    }
}
