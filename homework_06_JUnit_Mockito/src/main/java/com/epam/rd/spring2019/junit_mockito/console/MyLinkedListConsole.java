package com.epam.rd.spring2019.junit_mockito.console;

import com.epam.rd.spring2019.junit_mockito.core.impl.MyLinkedListImpl;

public class MyLinkedListConsole {

    public static void workWithMyLinkedList() {
        MyLinkedListImpl<Integer> myLinkedList = new MyLinkedListImpl<>();
        for (int i = 0; i < 10; i++) {
            myLinkedList.addLast(i);
        }
        myLinkedList.displayList();

        System.out.println("Add the first element \"100\"");
        myLinkedList.addFirst(100);
        myLinkedList.displayList();

        System.out.println("Add the element \"33\" after \"key - 3\" ");
        myLinkedList.addAfter(3, 33);
        myLinkedList.displayList();

        System.out.println("Add the last element \"500\"");
        myLinkedList.addLast(500);
        myLinkedList.displayList();

        System.out.println("Deleted the first element \"100\"");
        myLinkedList.removeFirst();
        myLinkedList.displayList();

        System.out.println("Deleted the element with \"key - 33\"");
        myLinkedList.remove(33);
        myLinkedList.displayList();

        System.out.println("Deleted the las element \"500\"");
        myLinkedList.removeLast();
        myLinkedList.displayList();

        System.out.println("The first element of MyLinkedList is: " + myLinkedList.getFirst());
        System.out.println("The element through \"key\" 7 of MyLinkedList is: " + myLinkedList.get(7));
        System.out.println("The last element of MyLinkedList is: " + myLinkedList.getLast());

        myLinkedList.size();
        System.out.println("The size of MyLinkedList is: " + myLinkedList.size());
        System.out.println("");

        MyLinkedListImpl<Integer> myLinkedList1 = new MyLinkedListImpl<>(5);
        int sizeOfMyLinkedList1 = myLinkedList1.size();
        for (int i = 0; i < sizeOfMyLinkedList1; i++) {
            myLinkedList1.removeFirst();
        }
        for (int j = 0; j < sizeOfMyLinkedList1; j++) {
            myLinkedList1.addLast(j);
        }

        myLinkedList1.displayList();
        myLinkedList.size();
        System.out.println("The size of MyLinkedList1 is: " + myLinkedList1.size());

    }

}
