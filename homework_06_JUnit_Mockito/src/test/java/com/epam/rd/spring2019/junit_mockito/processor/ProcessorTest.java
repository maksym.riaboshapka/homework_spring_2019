package com.epam.rd.spring2019.junit_mockito.processor;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProcessorTest {

    //@Mock annotation is used to create the mock object to be injected
    @Mock
    Producer producer;

    @Mock
    Consumer consumer;

    //@InjectMocks annotation is used to create and inject the mock object
    @InjectMocks
    Processor processor = new Processor();


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testProcess() {
        //GIVEN
        String expected = "Magic value";
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        when(producer.produce()).thenReturn(expected);
        //WHEN
        processor.process();
        //THEN
        verify(consumer).consume(argumentCaptor.capture());
        Assert.assertEquals(expected, argumentCaptor.getValue());

    }

    @Test
    public void testProduce() {
        //GIVEN
        when(producer.produce()).thenCallRealMethod();
        //WHEN
        processor.process();
        //THEN
        verify(producer, times(1)).produce();
        verifyNoMoreInteractions(producer);
    }


    @Test
    public void testConsumer() {
        //GIVEN
        String expected = "Magic value";
        when(producer.produce()).thenReturn(expected);
        //WHEN
        processor.process();
        //THEN
        verify(producer, times(1)).produce();
        verifyNoMoreInteractions(producer);
    }

    @Test(expected = IllegalStateException.class)
    public void produceThrowsIllegalStateException() {
        //GIVEN
        when(producer.produce()).thenReturn(null);
        //WHEN
        processor.process();
        //THEN
        verify(producer, times(1)).produce();
    }
}