package com.epam.rd.spring2019.junit_mockito.core.impl;

import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.rules.ErrorCollector;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

public class MyLinkedListImplTest {

    private MyLinkedListImpl<Integer> testLinkedList;

    @Rule
    public ErrorCollector collector = new ErrorCollector();

    @Before
    public void setUp() throws Exception {
        testLinkedList = new MyLinkedListImpl<>();
        for (int i = 0; i < 10; i++) {
            testLinkedList.addLast(i);
        }

    }

    @After
    public void tearDown() throws Exception {
        testLinkedList = null;
    }

    @Test
    public void testSize() {
        //GIVEN
        int expected = 10;
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        //WHEN
        int result = testLinkedList.size();
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);
        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(1));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 5));
        Assert.assertThat(list, Matchers.hasSize(10));
        //Assert.assertThat(list, Matchers.hasSize(3));     // for negative scenario

        collector.checkThat("Must be the same", result, is(10));
        //collector.checkThat("Must be the same", result, is(7));   // for negative scenario
    }

    @Test
    public void testAddFirst() {
        //GIVEN
        int expected = 100;
        List<Integer> list = Arrays.asList(100, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        //WHEN
        testLinkedList.addFirst(100);
        int result = testLinkedList.getFirst();
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);
        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(100));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 5, 100));
        Assert.assertThat(list, Matchers.hasSize(11));
        //Assert.assertThat(list, Matchers.hasSize(5));     // for negative scenario

        collector.checkThat("Must be the same", result, is(100));
        //collector.checkThat("Must be the same", result, is(7));   // for negative scenario
    }

    @Test
    public void testAddAfter() {
        //GIVEN
        int expected = 1;
        List<Integer> list = Arrays.asList(100, 0, 1, 2, 3, 33, 4, 5, 6, 7, 8, 9);
        //WHEN
        testLinkedList.addAfter(1, 33);
        int result = testLinkedList.get(1);
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);
        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(33));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 5, 33));
        Assert.assertThat(list, Matchers.hasSize(12));
        //Assert.assertThat(list, Matchers.hasSize(5));     // for negative scenario

        collector.checkThat("Must be the same", result, is(1));
        //collector.checkThat("Must be the same", result, is(7));   // for negative scenario
    }

    @Test
    public void testAddLast() {
        //GIVEN
        int expected = 500;
        List<Integer> list = Arrays.asList(100, 0, 1, 2, 3, 33, 4, 5, 6, 7, 8, 9, 500);
        //WHEN
        testLinkedList.addLast(500);
        int result = testLinkedList.getLast();
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);
        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(500));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 5, 500));
        Assert.assertThat(list, Matchers.hasSize(13));
        //Assert.assertThat(list, Matchers.hasSize(5));     // for negative scenario

    }

    @Test
    public void testRemoveFirst() {
        //GIVEN
        int expected = 1;
        List<Integer> list = Arrays.asList(1, 2, 3, 33, 4, 5, 6, 7, 8, 9);
        //WHEN
        testLinkedList.removeFirst();
        int result = testLinkedList.getFirst();
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);
        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(1));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 5));
        Assert.assertThat(list, Matchers.hasSize(10));
        //Assert.assertThat(list, Matchers.hasSize(5));     // for negative scenario

        collector.checkThat("Must be the same", result, is(1));
        //collector.checkThat("Must be the same", result, is(7));   // for negative scenario
    }

    @Test
    public void testRemove() {
        //GIVEN
        int expected = 1;
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        //WHEN
        testLinkedList.remove(2);
        int result = testLinkedList.get(1);
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);
        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(1));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 5));
        Assert.assertThat(list, Matchers.hasSize(10));
        //Assert.assertThat(list, Matchers.hasSize(5));     // for negative scenario
    }

    @Test
    public void testRemoveLast() {
        //GIVEN
        int expected = 8;
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8);
        //WHEN
        testLinkedList.removeLast();
        int result = testLinkedList.getLast();
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);
        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(1));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 5));
        Assert.assertThat(list, Matchers.hasSize(9));
        //Assert.assertThat(list, Matchers.hasSize(5));     // for negative scenario

        collector.checkThat("Must be the same", result, is(8));
        //collector.checkThat("Must be the same", result, is(7));   // for negative scenario
    }

    @Test
    public void testGetFirst() {
        //GIVEN
        int expected = 0;
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        //WHEN
        int result = testLinkedList.getFirst();
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);
        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(1));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 5));
        Assert.assertThat(list, Matchers.hasSize(10));
        //Assert.assertThat(list, Matchers.hasSize(5));     // for negative scenario
    }

    @Test
    public void testGet() {
        //GIVEN
        int expected = 1;
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        //WHEN
        int result = testLinkedList.get(1);
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);
        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(1));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 5));
        Assert.assertThat(list, Matchers.hasSize(10));
        //Assert.assertThat(list, Matchers.hasSize(5));     // for negative scenario

        collector.checkThat("Must be the same", result, is(1));
        //collector.checkThat("Must be the same", result, is(7));   // for negative scenario
    }

    @Test
    public void testGetLast() {
        //GIVEN
        int expected = 9;
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        //WHEN
        int result = testLinkedList.getLast();
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);
        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(1));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 5));
        Assert.assertThat(list, Matchers.hasSize(10));
        //Assert.assertThat(list, Matchers.hasSize(5));     // for negative scenario

        collector.checkThat("Must be the same", result, is(9));
        //collector.checkThat("Must be the same", result, is(7));   // for negative scenario
    }

}