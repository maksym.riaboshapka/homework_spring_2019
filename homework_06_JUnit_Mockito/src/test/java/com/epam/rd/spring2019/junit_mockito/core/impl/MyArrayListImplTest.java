package com.epam.rd.spring2019.junit_mockito.core.impl;

import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.rules.ErrorCollector;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

public class MyArrayListImplTest {
    Integer[] intArray = {1, 2, 3, 4, 5};
    private MyArrayListImpl<Integer> testArrayList;

    @Rule
    public ErrorCollector collector = new ErrorCollector();

    @Before
    public void setUp() throws Exception {
        testArrayList = new MyArrayListImpl<>(intArray.length);
        testArrayList.add(1);
        testArrayList.add(2);
        testArrayList.add(3);
        testArrayList.add(4);
        testArrayList.add(5);
    }

    @After
    public void tearDown() throws Exception {
        testArrayList = null;
    }

    @Test
    public void testSize() {
        //GIVEN
        int expected = 5;
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        //WHEN
        int result = testArrayList.size();
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);

        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(1));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 5));
        Assert.assertThat(list, Matchers.hasSize(5));
        //Assert.assertThat(list, Matchers.hasSize(3));     // for negative scenario

        collector.checkThat("Must be the same", result, is(5));
        //collector.checkThat("Must be the same", result, is(7));   // for negative scenario
    }

    @Test
    public void testAdd() {
        //GIVEN
        //WHEN
        boolean result = testArrayList.add(6);
        //THEN
        Assert.assertTrue(result);
    }

    @Test
    public void testAddWithArguments() {
        //GIVEN
        int expected = 50;
        List<Integer> list = Arrays.asList(1, 2, 3, 50, 4, 5);
        //WHEN
        testArrayList.add(3, 50);
        int result = testArrayList.get(3);
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);

        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(50));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 5, 50));
        Assert.assertThat(list, Matchers.hasSize(6));
        //Assert.assertThat(list, Matchers.hasSize(5));     // for negative scenario

        collector.checkThat("Must be the same", result, is(50));
        //collector.checkThat("Must be the same", result, is(5));      // for negative scenario
    }

    @Test
    public void testSet() {
        //GIVEN
        int expected = 100;
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 100);
        //WHEN
        testArrayList.set(4, 100);
        int result = testArrayList.get(4);
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);

        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(100));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 100));
        Assert.assertThat(list, Matchers.hasSize(5));
        //Assert.assertThat(list, Matchers.hasSize(6));     // for negative scenario

        collector.checkThat("Must be the same", result, is(100));
        //collector.checkThat("Must be the same", result, is(5));   // for negative scenario
    }

    @Test
    public void testGet() {
        //GIVEN
        int expected = 3;
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        //WHEN
        int result = testArrayList.get(2);
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);

        Assert.assertThat(list, Matchers.hasItem(3));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasSize(5));
        //Assert.assertThat(list, Matchers.hasSize(3));     // for negative scenario

        collector.checkThat("Must be the same", result, is(3));
        //collector.checkThat("Must be the same", result, is(5));   // for negative scenario
    }

    @Test
    public void testRemove() {
        //GIVEN
        int expected = 5;
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        //WHEN
        testArrayList.remove(3);
        int result = testArrayList.get(3);
        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);

        Assert.assertThat(list, Matchers.not(Matchers.empty()));
        Assert.assertThat(list, Matchers.notNullValue());
        Assert.assertThat(list, Matchers.hasItem(3));
        //Assert.assertThat(list, Matchers.hasItem(9));     // for negative scenario
        Assert.assertThat(list, Matchers.hasItems(1, 3, 5));
        Assert.assertThat(list, Matchers.hasSize(5));
        //Assert.assertThat(list, Matchers.hasSize(6));     // for negative scenario

        collector.checkThat("Must be the same", result, is(5));
        //collector.checkThat("Must be the same", result, is(3));   // for negative scenario
    }

}