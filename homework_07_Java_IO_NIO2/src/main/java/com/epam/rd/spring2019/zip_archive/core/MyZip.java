package com.epam.rd.spring2019.zip_archive.core;

import java.io.File;
import java.io.IOException;
import java.util.zip.ZipOutputStream;

public interface MyZip {

    /**
     * create a zip-archive of a folder with files inside
     *
     * @param fileToZip a file to zip
     * @param fileName  a name of file
     * @param zipOut    a stream of files to write into zip-archive
     * @return true if zip-archive is created
     * @throws IOException if an I/O error has occurred
     */
    boolean zipping(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException;

    /**
     * @param sourceWithZipArchive for reading all files
     * @return true if zip-archive is read
     */
    boolean readZipArchive(String sourceWithZipArchive);

    /**
     * @param sourceWithZipArchive source which contains zip-archive
     * @param sourceForUnzipping   source for unzipping of zip-archive
     * @return true if zip-archive is unzipped
     */
    boolean unzipping(String sourceWithZipArchive, String sourceForUnzipping);
}
