package com.epam.rd.spring2019.zip_archive;

import com.epam.rd.spring2019.zip_archive.console.MyUnzipArchive;
import com.epam.rd.spring2019.zip_archive.console.MyZipArchive;

import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {

        MyZipArchive.workWithMyZipArchive();
        MyUnzipArchive.workWithMyUnzipArchive();

    }
}