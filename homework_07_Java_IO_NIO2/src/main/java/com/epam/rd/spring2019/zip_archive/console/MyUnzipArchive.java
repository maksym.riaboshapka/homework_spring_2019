package com.epam.rd.spring2019.zip_archive.console;

import com.epam.rd.spring2019.zip_archive.core.MyZip;
import com.epam.rd.spring2019.zip_archive.core.impl.MyZipImpl;

public class MyUnzipArchive {

    private static final String PATH_TO_ARCHIVE =
            "homework_07_Java_IO_NIO2" +
                    "/src" +
                    "/main" +
                    "/resources/";

    public static void workWithMyUnzipArchive() {

        String sourceWithZipArchive = PATH_TO_ARCHIVE + "myZipArchive.zip";

        String sourceForUnzipping = PATH_TO_ARCHIVE + "folderForUnzipping/";


        MyZip myZip = new MyZipImpl();
        myZip.readZipArchive(sourceWithZipArchive);
        myZip.unzipping(sourceWithZipArchive, sourceForUnzipping);
    }

}
