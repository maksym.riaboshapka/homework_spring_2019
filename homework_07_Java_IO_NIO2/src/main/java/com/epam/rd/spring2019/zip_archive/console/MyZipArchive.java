package com.epam.rd.spring2019.zip_archive.console;

import com.epam.rd.spring2019.zip_archive.core.MyZip;
import com.epam.rd.spring2019.zip_archive.core.impl.MyZipImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipOutputStream;

public class MyZipArchive {

    private static final String PATH_TO_ARCHIVE =
            "homework_07_Java_IO_NIO2" +
                    "/src" +
                    "/main" +
                    "/resources/";


    public static void workWithMyZipArchive() throws IOException {
        String sourceFile = PATH_TO_ARCHIVE + "foldersAndFilesForZipping";
        FileOutputStream fileOutputStream = new FileOutputStream(PATH_TO_ARCHIVE +
                "myZipArchive.zip");
        ZipOutputStream zipOut = new ZipOutputStream(fileOutputStream);
        File fileToZip = new File(sourceFile);

        MyZip myZip = new MyZipImpl();
        myZip.zipping(fileToZip, fileToZip.getName(), zipOut);
        zipOut.close();
        fileOutputStream.close();

    }

}
