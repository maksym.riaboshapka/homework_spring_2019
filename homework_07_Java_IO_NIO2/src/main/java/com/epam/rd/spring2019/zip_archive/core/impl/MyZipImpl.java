package com.epam.rd.spring2019.zip_archive.core.impl;

import com.epam.rd.spring2019.zip_archive.core.MyZip;

import java.io.*;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class MyZipImpl implements MyZip {

    @Override
    public boolean zipping(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {
        if (fileToZip.isHidden()) {
            return false;
        }
        if (fileToZip.isDirectory()) {
            if (fileName.endsWith("/")) {
                zipOut.putNextEntry(new ZipEntry(fileName));
                zipOut.closeEntry();
            } else {
                zipOut.putNextEntry(new ZipEntry(fileName + "/"));
                zipOut.closeEntry();
            }
            File[] children = fileToZip.listFiles();
            for (File childFile : children) {
                zipping(childFile, fileName + "/" + childFile.getName(), zipOut);
            }
            return true;
        }
        FileInputStream fis = new FileInputStream(fileToZip);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zipOut.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
        fis.close();
        return true;
    }

    @Override
    public boolean readZipArchive(String sourceWithZipArchive) {
        try (ZipInputStream zipInputStream =
                     new ZipInputStream(new FileInputStream(sourceWithZipArchive));
             ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            ZipEntry zipEntry;
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                copyData(zipInputStream, byteArrayOutputStream);
                System.out.printf("Name: %s \t size: %d %n",
                        zipEntry.getName(), zipEntry.getSize());
            }
            return true;

        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
            return false;
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return false;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean unzipping(String sourceWithZipArchive, String sourceForUnzipping) {
        try (ZipFile file = new ZipFile(sourceWithZipArchive)) {
            FileSystem fileSystem = FileSystems.getDefault();
            //Get file entries
            Enumeration<? extends ZipEntry> entries = file.entries();

            File unzipDirectory = new java.io.File(sourceForUnzipping);
            if (!unzipDirectory.exists()) {
                Files.createDirectory(fileSystem.getPath(sourceForUnzipping));
            }

            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                //If directory then create a new directory in uncompressed folder
                if (entry.isDirectory()) {
                    System.out.println("\nCreating Directory:" +
                            sourceForUnzipping + entry.getName());
                    Files.createDirectories(fileSystem.getPath(sourceForUnzipping +
                            entry.getName()));
                }
                //Else create the file
                else {
                    InputStream is = file.getInputStream(entry);
                    BufferedInputStream bis = new BufferedInputStream(is);
                    String uncompressedFileName = sourceForUnzipping + entry.getName();
                    Path uncompressedFilePath = fileSystem.getPath(uncompressedFileName);
                    File unzipFile = new java.io.File(uncompressedFileName);
                    if (!unzipFile.exists()) {
                        Files.createFile(uncompressedFilePath);
                    }
                    FileOutputStream fileOutput = new FileOutputStream(uncompressedFileName);
                    while (bis.available() > 0) {
                        fileOutput.write(bis.read());
                    }
                    fileOutput.close();
                    System.out.println("Written :" + entry.getName());
                }
            }
            return true;
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    private static void copyData(InputStream inputStream, OutputStream outputStream) throws Exception {
        while (inputStream.available() > 0) {
            outputStream.write(inputStream.read());
        }
    }

}
