package com.epam.rd.spring2019.zip_archive.core.impl;

import com.epam.rd.spring2019.zip_archive.core.MyZip;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipOutputStream;

public class MyZipImplTest {

    private static final String PATH_TO_ARCHIVE = "src/main/resources/";

    private String sourceFile;
    private FileOutputStream fileOutputStream;
    private String sourceWithZipArchive;
    private String sourceForUnzipping;

    private ZipOutputStream zipOut;
    private File fileToZip;
    private MyZip myZip;

    @Before
    public void setUp() throws Exception {
        sourceFile = PATH_TO_ARCHIVE + "foldersAndFilesForZipping";
        fileOutputStream = new FileOutputStream(PATH_TO_ARCHIVE + "myZipArchive.zip");
        sourceWithZipArchive = PATH_TO_ARCHIVE + "myZipArchive.zip";
        sourceForUnzipping = PATH_TO_ARCHIVE + "folderForUnzipping/";

        zipOut = new ZipOutputStream(fileOutputStream);
        fileToZip = new File(sourceFile);
        myZip = new MyZipImpl();
    }

    @After
    public void tearDown() throws Exception {
        sourceFile = null;
        fileToZip = null;
        myZip = null;
        zipOut.close();
        fileOutputStream.close();
    }

    @Test
    public void testZipping() throws IOException {
        //GIVEN
        //WHEN
        boolean result = myZip.zipping(fileToZip, fileToZip.getName(), zipOut);
        //THEN
        Assert.assertTrue(result);
    }

    @Test
    public void testReadZipArchive() {
        //GIVEN
        //WHEN
        boolean result = myZip.readZipArchive(sourceWithZipArchive);
        //THEN
        Assert.assertTrue(result);
    }

    @Test
    public void unzipping() {
        //GIVEN
        //WHEN
        boolean result = myZip.unzipping(sourceWithZipArchive, sourceForUnzipping);
        //THEN
        Assert.assertFalse(result);
    }
}