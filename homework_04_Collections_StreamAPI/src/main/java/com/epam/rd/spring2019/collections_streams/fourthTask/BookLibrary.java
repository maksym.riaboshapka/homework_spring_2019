package com.epam.rd.spring2019.collections_streams.fourthTask;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BookLibrary {

    private static final String V_GUGO = "В.Гюго";
    private static final String O_DYUMA = "О.Дюма";

    public static void workWithBookLibrary() {
        System.out.println("\nThe fourth task:\n");

        Collection<Book> booksCollection = Arrays.asList(
                new Book(V_GUGO, "Собор Паризької Богоматері", "isbn:978-966-8602-34-4"),
                new Book(V_GUGO, "Знедолені", "isbn:934-506-5938-34-5"),
                new Book(O_DYUMA, "Три мушкитери", "isbn:985-098-9038-77-9"),
                new Book(O_DYUMA, "Двадцять років потому", "isbn:923-867-6034-89-6"),
                new Book(O_DYUMA, "Граф Монте-Крісто", "isbn:989-074-698-54-9"));

        booksCollection.stream().forEach(p -> System.out.println(p.toString()));

        System.out.println();

        Map<String, Long> booksOfAuthor = booksCollection.stream().collect(
                Collectors.groupingBy(Book::getAuthor, Collectors.counting()));

        System.out.println("The book's amount of each author is:");
        for (Map.Entry<String, Long> item : booksOfAuthor.entrySet()) {
            System.out.println(item.getKey() + " - " + item.getValue());
        }

        System.out.println("\nAdded a new book: \"" + O_DYUMA
                + "\", \"Дві Діани\", \"isbn:958-834-7539-89-4\"\n");

        Stream<Book> newBookstream = Stream.of(new Book(O_DYUMA,
                "Дві Діани", "isbn:958-834-7539-89-4"));
        Stream<Book> concatBookStream = Stream.concat(booksCollection.stream(), newBookstream);

        Map<String, Long> newBooksOfAuthor = concatBookStream.collect(
                Collectors.groupingBy(Book::getAuthor, Collectors.counting()));

        System.out.println("The new list of library is: ");
        for (Map.Entry<String, Long> item1 : newBooksOfAuthor.entrySet()) {
            System.out.println(item1.getKey() + " - " + item1.getValue());
        }

        System.out.println();
    }

}
