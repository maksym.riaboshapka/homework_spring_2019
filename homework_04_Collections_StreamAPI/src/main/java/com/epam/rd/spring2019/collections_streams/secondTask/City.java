package com.epam.rd.spring2019.collections_streams.secondTask;

public class City {
    private String countryOrState;
    private String cityOfCountryOrState;
    private long population;

    public City(String countryOrState, String cityOfCountryOrState, long population) {
        this.countryOrState = countryOrState;
        this.cityOfCountryOrState = cityOfCountryOrState;
        this.population = population;
    }

    public String getCountryOrState() {
        return countryOrState;
    }

    public String getCityOfCountryOrState() {
        return cityOfCountryOrState;
    }

    public long getPopulation() {
        return population;
    }

    @Override
    public String toString() {
        return "City{"
                + "countryOrState='" + countryOrState + '\''
                + ", cityOfCountryOrState='" + cityOfCountryOrState + '\''
                + ", population=" + population + '}';
    }

}