package com.epam.rd.spring2019.collections_streams.thirdTask;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Zipper {

    public static void workWithZipper() {
        System.out.println("\nThe third task:\n");

        Stream firsStream = Arrays.asList(5, 8, 3, 9, 5).stream();
        Stream secondStream = Arrays.asList(6, 3, 4, 9).stream();

        Stream expectedStream = zip(firsStream, secondStream);
        expectedStream.forEach(es -> System.out.print(es + " "));

        System.out.println();
    }

    private static <T> Stream<T> zip(Stream<T> first, Stream<T> second) {
        if ((first != null && second != null)) {
            List<T> firstList = first.collect(Collectors.toList());
            List<T> secondList = second.collect(Collectors.toList());

            Stream.Builder<T> newStream = Stream.builder();
            int count = 0;
            if (firstList.size() <= secondList.size()) {
                count = firstList.size();
            } else if (secondList.size() <= firstList.size()) {
                count = secondList.size();
            }

            IntStream.range(0, count).forEach(s -> {
                newStream.add(firstList.get(s));
                newStream.add(secondList.get(s));
            });
            return newStream.build();
        } else {
            throw new NullPointerException();
        }
    }

}
