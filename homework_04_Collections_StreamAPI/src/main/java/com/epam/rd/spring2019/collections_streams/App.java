package com.epam.rd.spring2019.collections_streams;

import com.epam.rd.spring2019.collections_streams.firstTask.CommaSeparatedString;
import com.epam.rd.spring2019.collections_streams.fourthTask.BookLibrary;
import com.epam.rd.spring2019.collections_streams.secondTask.LargestCityOfState;
import com.epam.rd.spring2019.collections_streams.thirdTask.Zipper;

public class App {

    public static void main(String[] args) {

        CommaSeparatedString.workWithCommaSeparatedString();
        LargestCityOfState.workWithLargestCity();
        Zipper.workWithZipper();
        BookLibrary.workWithBookLibrary();


    }

}
