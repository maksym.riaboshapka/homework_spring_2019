package com.epam.rd.spring2019.collections_streams.secondTask;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LargestCityOfState {

    private static final String UKRAINE = "Ukraine";
    private static final String USA = "USA";
    private static final String MEXICO = "Mexico";
    private static final String DNIPRO = "Dnipro";
    private static final String KYIV = "Kyiv";
    private static final String LVIV = "Lviv";
    private static final String NEW_YORK = "New-York";
    private static final String MEXICO_CITY = "Mexico City";
    private static final String LOS_ANGELES = "Los Angeles";
    private static final String PUEBLA = "Puebla";
    private static final int POPULATION_OF_DNIPRO = 1_065_000;
    private static final int POPULATION_OF_KYIV = 2_611_000;
    private static final int POPULATION_OF_LVIV = 732_000;
    private static final int POPULATION_OF_NEW_YORK = 8_620_000;
    private static final int POPULATION_OF_MEXICO_CITY = 8_850_000;
    private static final int POPULATION_OF_LOS_ANGELES = 3_999_000;
    private static final int POPULATION_OF_PUEBLA = 1_434_000;

    public static void workWithLargestCity() {
        System.out.println("\nThe second task:\n");


        Stream<City> cityStreamForPrint = Stream.of(
                new City(UKRAINE, DNIPRO, POPULATION_OF_DNIPRO),
                new City(UKRAINE, KYIV, POPULATION_OF_KYIV),
                new City(UKRAINE, LVIV, POPULATION_OF_LVIV),
                new City(USA, NEW_YORK, POPULATION_OF_NEW_YORK),
                new City(MEXICO, MEXICO_CITY, POPULATION_OF_MEXICO_CITY),
                new City(USA, LOS_ANGELES, POPULATION_OF_LOS_ANGELES),
                new City(MEXICO, PUEBLA, POPULATION_OF_PUEBLA));

        cityStreamForPrint.forEach(p -> System.out.println(p.toString()));

        System.out.println();

        Collection<City> citiesOfCountryOrState = Arrays.asList(
                new City(UKRAINE, DNIPRO, POPULATION_OF_DNIPRO),
                new City(UKRAINE, KYIV, POPULATION_OF_KYIV),
                new City(UKRAINE, LVIV, POPULATION_OF_LVIV),
                new City(USA, NEW_YORK, POPULATION_OF_NEW_YORK),
                new City(MEXICO, MEXICO_CITY, POPULATION_OF_MEXICO_CITY),
                new City(USA, LOS_ANGELES, POPULATION_OF_LOS_ANGELES),
                new City(MEXICO, PUEBLA, POPULATION_OF_PUEBLA));

        Map<String, Optional<City>> citiesOfCountry =
                getLargestCityPerState(citiesOfCountryOrState);
        for (Map.Entry<String, Optional<City>> item : citiesOfCountry.entrySet()) {
            System.out.println(item.getKey() + " - "
                    + item.getValue().get().getCityOfCountryOrState()
                    + ", " + item.getValue().get().getPopulation());
        }

        System.out.println();

    }

    private static Map<String, Optional<City>> getLargestCityPerState(Collection<City> cityStream) {
        return cityStream.stream().collect(Collectors.groupingBy(City::getCountryOrState,
                Collectors.maxBy(Comparator.comparing(City::getPopulation))));
    }

}
