package com.epam.rd.spring2019.collections_streams.firstTask;

import java.util.ArrayList;
import java.util.stream.Stream;

public class CommaSeparatedString {

    public static void workWithCommaSeparatedString() {

        System.out.println("\nThe first task is:\n");
        ArrayList<Integer> myIntList = new ArrayList<>();
        myIntList.add(7);
        myIntList.add(12);
        myIntList.add(3);
        myIntList.add(20);
        myIntList.add(25);
        myIntList.add(27);
        myIntList.add(4);

        System.out.println("myIntList is: " + myIntList);
        Stream<Integer> myIntStream = myIntList.stream();
        myIntStream.map(p -> {
            if ((p % 2) == 0) return "e" + p;
            else return "o" + p;
        })
                .forEach(s -> System.out.print(s + ","));

        System.out.println();
    }

}
