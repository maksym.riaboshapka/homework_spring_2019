package com.epam.rd.spring2019.jdbc_sql_liquibase.dao;

import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.User;

import java.util.List;

public interface UserDao {

    /**
     * save user
     *
     * @param user for save
     * @return true if user is saved
     */
    boolean saveUser(User user);

    /**
     * modify user
     *
     * @param id   of user for modify
     * @param user for modify
     * @return true if user is modified
     */
    boolean modifyUser(long id, User user);

    /**
     * to read a list of all users
     *
     * @return list of all users
     */
    List<User> getAllUsers();

    /**
     * delete user by Id
     *
     * @param id of user
     * @return true if user was deleted
     */
    boolean deleteUser(long id);

}