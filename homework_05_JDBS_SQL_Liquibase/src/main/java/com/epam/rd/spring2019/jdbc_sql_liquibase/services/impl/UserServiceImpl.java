package com.epam.rd.spring2019.jdbc_sql_liquibase.services.impl;

import com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.UserDBDao;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.User;
import com.epam.rd.spring2019.jdbc_sql_liquibase.services.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {

    private UserDBDao userDBDao;

    public UserServiceImpl(UserDBDao userDBDao) {
        this.userDBDao = userDBDao;
    }

    @Override
    public void createUser(String name, String surname) {
        User user = new User(name, surname);
        boolean result = userDBDao.saveUser(user);
        if (result) {
            System.out.println("The user is saved: " + user);
        }

    }

    @Override
    public void modifyUser(long id, String name, String surname) {
        for (User user : getAllUsers()) {
            long userId = user.getId();
            if (userId == id) {
                user.setName(name);
                user.setSurname(surname);
                boolean result = userDBDao.modifyUser(id, user);
                if (result) {
                    System.out.println("The user is modified: " + user);
                }
            }
        }

    }

    @Override
    public List<User> getAllUsers() {
        return userDBDao.getAllUsers();
    }


    @Override
    public void deleteUser(long id) {
        for (User user : getAllUsers()) {
            long userId = user.getId();
            if (userId == id) {
                boolean result = userDBDao.deleteUser(id);
                if (result) {
                    System.out.println("The user is deleted: " + user);
                }
            }
        }
    }
}
