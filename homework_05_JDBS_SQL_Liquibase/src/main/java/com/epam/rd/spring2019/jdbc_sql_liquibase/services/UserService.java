package com.epam.rd.spring2019.jdbc_sql_liquibase.services;

import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.User;

import java.util.List;

public interface UserService {


    /**
     * create a user
     *
     * @param name    of the user
     * @param surname of the user
     */
    void createUser(String name, String surname);

    /**
     * a method for modify user
     *
     * @param id      of current user
     * @param name    of current user
     * @param surname of current user
     *                //     * @param phone   of current user
     */
    void modifyUser(long id, String name, String surname);

    /**
     * get all users
     *
     * @return a list of all users
     */
    List<User> getAllUsers();


    /**
     * delete the user
     *
     * @param id of the user
     */
    void deleteUser(long id);

}
