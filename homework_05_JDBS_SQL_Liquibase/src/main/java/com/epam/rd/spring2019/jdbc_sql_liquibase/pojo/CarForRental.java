package com.epam.rd.spring2019.jdbc_sql_liquibase.pojo;

import java.math.BigDecimal;
import java.util.Objects;

public class CarForRental {

    private long id;
    private String model;
    private BigDecimal price;

    public CarForRental() {
    }

    public CarForRental(String model, BigDecimal price) {
        this.model = model;
        this.price = price;
    }

    public CarForRental(long id, String model, BigDecimal price) {
        this.id = id;
        this.model = model;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "The car for rental{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarForRental carForRental = (CarForRental) o;
        return id == carForRental.id &&
                Objects.equals(model, carForRental.model) &&
                Objects.equals(price, carForRental.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, model, price);
    }
}
