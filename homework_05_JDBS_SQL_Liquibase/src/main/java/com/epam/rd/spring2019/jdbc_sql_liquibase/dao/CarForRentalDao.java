package com.epam.rd.spring2019.jdbc_sql_liquibase.dao;

import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.CarForRental;

import java.util.List;

public interface CarForRentalDao {

    /**
     * save the car for rental
     *
     * @param carForRental for save
     * @return true if the car for rental is saved
     */
    boolean saveCarForRental(CarForRental carForRental);


    /**
     * to read a list of all cars for rental
     *
     * @return list of all cars for rental
     */
    List<CarForRental> getAllCarsForRental();


    /**
     * modify the car for rental
     *
     * @param id           of the car for rental for modify
     * @param carForRental of the car for rental for modify
     * @return true if the car for rental is modified
     */
    boolean modifyCarForRental(long id, CarForRental carForRental);


    /**
     * delete the car for rental by Id
     *
     * @param id of the car for rental
     * @return true if the car for rental is deleted
     */
    boolean deleteCarForRental(long id);
}
