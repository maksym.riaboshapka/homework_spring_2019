package com.epam.rd.spring2019.jdbc_sql_liquibase.services;

import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.CarForRental;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.User;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.RentalOrder;

import java.util.List;

public interface RentalOrderService {

    /**
     * create the rental order
     *
     * @param user          for the rental order
     * @param carForRentals list of carForRentals for the rental order
     */
    void createRentalOrder(User user, List<CarForRental> carForRentals);


    /**
     * get all rental orders
     *
     * @return list of all rental orders
     */
    List<RentalOrder> getAllRentalOrders();

    /**
     * a method modify information about the rental order
     *
     * @param id                     of the rental order
     * @param userForModifyOrder     of the rental order
     * @param carsListForModifyOrder list of the cars for the rental order
     */
    void modifyRentalOrder(long id, User userForModifyOrder, List<CarForRental> carsListForModifyOrder);

    /**
     * delete the rental order by id
     *
     * @param id of the rental order
     */
    void deleteRentalOrder(long id);
}
