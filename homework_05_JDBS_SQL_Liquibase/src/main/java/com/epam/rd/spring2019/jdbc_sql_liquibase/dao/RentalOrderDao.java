package com.epam.rd.spring2019.jdbc_sql_liquibase.dao;

import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.RentalOrder;

import java.util.List;

public interface RentalOrderDao {

    /**
     * save the rental order
     *
     * @param rentalOrder for save
     * @return true is the rental order is saved
     */
    boolean saveRentalOrder(RentalOrder rentalOrder);


    /**
     * to read a list of all rental orders
     *
     * @return list of all rental orders
     */
    List<RentalOrder> getAllRentalOrders();


    /**
     * modify the rental order
     *
     * @param id          of the rental order for modify
     * @param rentalOrder of teh rental order for modify
     * @return true if the rental order is modified
     */
    boolean modifyRentalOrder(long id, RentalOrder rentalOrder);


    /**
     * delete the rental order by Id
     *
     * @param rentalOrderId of the rental order
     * @return true if the rental order was deleted
     */
    boolean deleteRentalOrder(long rentalOrderId);
}
