package com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl;

import com.epam.rd.spring2019.jdbc_sql_liquibase.dao.RentalOrderDao;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.CarForRental;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.RentalOrder;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.DataForConnectionToH2DB.*;
import static com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.SomeGeneralServiceMethodsForDB.findCarForRental;
import static com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.SomeGeneralServiceMethodsForDB.findUser;

public class RentalOrderDBDao implements RentalOrderDao {

    public RentalOrderDBDao() {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             Statement statement = connection.createStatement()) {
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS RENTAL_ORDERS(" +
                            "ID BIGINT DEFAULT 1 PRIMARY KEY AUTO_INCREMENT," +
                            " USER_ID BIGINT DEFAULT 1, CAR_ID BIGINT DEFAULT 1)");
        } catch (SQLException e) {
            System.out.println("SOMETHING IS GOING WRONG!!!");
        }
    }

    @Override
    public boolean saveRentalOrder(RentalOrder rentalOrder) {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "INSERT INTO RENTAL_ORDERS (USER_ID, CAR_ID) VALUES(?, ?)")) {
            System.out.println("Saving.... Please wait");
            for (CarForRental carForRental : rentalOrder.getCarForRentals()) {
                User user = rentalOrder.getUser();
                statement.setLong(1, user.getId());
                statement.setLong(2, carForRental.getId());
                statement.execute();
            }
            System.out.println("The rental order is saved: " + "The rental order{" +
                    "user=" + rentalOrder.getUser() +
                    ", cars=" + rentalOrder.getCarForRentals() +
                    '}');
        } catch (SQLException e) {
            System.out.println("SOMETHING OF SAVING WAS GOING WRONG!!!");
        }
        return false;
    }

    @Override
    public List<RentalOrder> getAllRentalOrders() {
        List<RentalOrder> resultRentalOrdersList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM RENTAL_ORDERS")) {
            while (resultSet.next()) {
                long rentalOrderID = resultSet.getLong("ID");
                User user = getUser(resultSet);
                CarForRental carForRental = getCarForRental(resultSet);
                List<CarForRental> CarsForRentalList = new ArrayList<>();
                CarsForRentalList.add(carForRental);
                resultRentalOrdersList.add(new RentalOrder(rentalOrderID, user, CarsForRentalList));
            }
        } catch (SQLException e) {
            System.out.println("THE RENTAL ORDERS DIDN'T FIND!!!");
        }
        return resultRentalOrdersList;
    }

    @Override
    public boolean modifyRentalOrder(long id, RentalOrder rentalOrder) {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "UPDATE RENTAL_ORDERS SET USER_ID = ?, CAR_ID = ? WHERE ID = ?")) {
            System.out.println("Modifying.... Please wait");
            statement.setLong(3, id);
            User user = rentalOrder.getUser();
            statement.setLong(1, user.getId());
            for (CarForRental carForRental : rentalOrder.getCarForRentals()) {
                statement.setLong(2, carForRental.getId());
                break;
            }
            System.out.println("The rental order is modified: " + rentalOrder);
            statement.execute();
        } catch (SQLException e) {
            System.out.println("SOMETHING WAS GOING WRONG!!! THE RENTAL ORDER DIDN'T FIND FOR MODIFYING!!!");
        }
        return false;
    }

    @Override
    public boolean deleteRentalOrder(long rentalOrderId) {
        RentalOrder rentalOrderForDelete = findRentalOrder(rentalOrderId);
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "DELETE FROM RENTAL_ORDERS WHERE ID = ?")) {
            System.out.println("Deleting... Please wait");
            statement.setLong(1, rentalOrderId);
            System.out.println("The rental order is deleted: " + rentalOrderForDelete);
            statement.execute();
        } catch (SQLException e) {
            System.out.println("SOMETHING WAS GOING WRONG!!! THE RENTAL ORDER DIDN'T FIND FOR DELETING!!!");
        }
        return false;
    }

    private RentalOrder findRentalOrder(long rentalOrderId) {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM RENTAL_ORDERS WHERE ID = ?")) {
            statement.setLong(1, rentalOrderId);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            long id = resultSet.getLong("ID");
            User user = getUser(resultSet);
            CarForRental carForRental = getCarForRental(resultSet);
            List<CarForRental> carsForRentalList = new ArrayList<>();
            carsForRentalList.add(carForRental);
            resultSet.close();
            return new RentalOrder(id, user, carsForRentalList);
        } catch (SQLException e) {
            System.out.println("THE RENTAL ORDER DIDN'T FIND!!!");
        }
        return null;
    }

    private User getUser(ResultSet resultSet) throws SQLException {
        long userId = resultSet.getLong("USER_ID");
        return findUser(userId);
    }

    private CarForRental getCarForRental(ResultSet resultSet) throws SQLException {
        long carID = resultSet.getLong("CAR_ID");
        return findCarForRental(carID);
    }

}
