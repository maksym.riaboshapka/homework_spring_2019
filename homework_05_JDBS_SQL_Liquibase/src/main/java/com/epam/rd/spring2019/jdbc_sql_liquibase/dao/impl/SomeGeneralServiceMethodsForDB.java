package com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl;

import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.CarForRental;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.User;

import java.math.BigDecimal;
import java.sql.*;

import static com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.DataForConnectionToH2DB.*;

public class SomeGeneralServiceMethodsForDB {

    public static User findUser(long userId) {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement =
                     connection.prepareStatement("SELECT * FROM USERS WHERE ID = ?")) {
            statement.setLong(1, userId);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            long id = resultSet.getLong("ID");
            String name = resultSet.getString("NAME");
            String surname = resultSet.getString("SURNAME");
            resultSet.close();
            return new User(id, name, surname);
        } catch (SQLException e) {
            System.out.println("USER DIDN'T FIND!!!");
        }
        return null;
    }

    public static CarForRental findCarForRental(long carId) {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement =
                     connection.prepareStatement("SELECT * FROM CARS WHERE ID = ?")) {
            statement.setLong(1, carId);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            long id = resultSet.getLong("ID");
            String model = resultSet.getString("MODEL");
            BigDecimal price = resultSet.getBigDecimal("PRICE");
            resultSet.close();
            return new CarForRental(id, model, price);
        } catch (SQLException e) {
            System.out.println("CAR DIDN'T FIND!!!");
        }
        return null;
    }


}
