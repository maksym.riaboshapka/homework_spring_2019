package com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl;

import com.epam.rd.spring2019.jdbc_sql_liquibase.dao.UserDao;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.DataForConnectionToH2DB.*;
import static com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.SomeGeneralServiceMethodsForDB.findUser;

public class UserDBDao implements UserDao {

    public UserDBDao() {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             Statement statement = connection.createStatement()) {

            statement.execute(
                    "CREATE TABLE IF NOT EXISTS USERS(" +
                            "ID BIGINT DEFAULT 1 PRIMARY KEY AUTO_INCREMENT," +
                            " NAME VARCHAR(20) DEFAULT NULL, SURNAME VARCHAR(20) DEFAULT NULL)"
            );

        } catch (SQLException e) {
            System.out.println("WHEN CREATING A TABLE, SOMETHING WENT WRONG!!!");
        }
    }

    @Override
    public boolean saveUser(User user) {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "INSERT INTO USERS (NAME, SURNAME) VALUES(?, ?)")) {
            System.out.println("Saving.... Please wait");
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            System.out.println("User Saved: " + "User{" +
                    "name='" + user.getName() + '\'' +
                    ", surname='" + user.getSurname() + '\'' +
                    '}');
            preparedStatement.execute();
        } catch (SQLException e) {
            System.out.println("WHEN SAVING A USER, SOMETHING WENT WRONG!!!");
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean modifyUser(long id, User user) {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "UPDATE USERS SET NAME = ?, SURNAME = ? WHERE ID = ?")) {
            System.out.println("Modifying.... Please wait");
            statement.setLong(3, id);
            statement.setString(1, user.getName());
            statement.setString(2, user.getSurname());
            System.out.println("User Modified: " + user);
            statement.execute();
        } catch (SQLException e) {
            System.out.println("SOMETHING WAS GOING WRONG!!! USER DIDN'T FIND FOR MODIFYING!!!");
        }
        return false;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> resultUsersList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM USERS")) {
            while (resultSet.next()) {
                long id = resultSet.getLong("ID");
                String name = resultSet.getString("NAME");
                String surname = resultSet.getString("SURNAME");
                resultUsersList.add(new User(id, name, surname));
            }
        } catch (SQLException e) {
            System.out.println("USERS DIDN'T FIND!!!");
        }
        return resultUsersList;
    }

    @Override
    public boolean deleteUser(long userId) {
        User userForDelete = findUser(userId);
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "DELETE FROM USERS WHERE ID = ?")) {
            System.out.println("Deleting... Please wait");
            statement.setLong(1, userId);
            System.out.println("User Deleted: " + userForDelete);
            statement.execute();
        } catch (SQLException e) {
            System.out.println("SOMETHING WAS GOING WRONG!!! USER DIDN'T FIND FOR DELETING!!!");
        }
        return false;
    }

}
