package com.epam.rd.spring2019.jdbc_sql_liquibase.pojo;

import java.util.List;
import java.util.Objects;

public class RentalOrder {

    private long id;
    private User user;
    private List<CarForRental> carForRentals;

    public RentalOrder() {
    }

    public RentalOrder(User user, List<CarForRental> carForRentals) {
        this.user = user;
        this.carForRentals = carForRentals;
    }

    public RentalOrder(long id, User user, List<CarForRental> carForRentals) {
        this.id = id;
        this.user = user;
        this.carForRentals = carForRentals;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<CarForRental> getCarForRentals() {
        return carForRentals;
    }

    public void setCarForRentals(List<CarForRental> carForRentals) {
        this.carForRentals = carForRentals;
    }

    @Override
    public String toString() {
        return "The rental order{" +
                "id=" + id +
                ", user=" + user +
                ", the car for rental=" + carForRentals +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RentalOrder rentalOrder = (RentalOrder) o;
        return id == rentalOrder.id &&
                Objects.equals(user, rentalOrder.user) &&
                Objects.equals(carForRentals, rentalOrder.carForRentals);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, carForRentals);
    }
}
