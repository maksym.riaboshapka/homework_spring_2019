package com.epam.rd.spring2019.jdbc_sql_liquibase.services.impl;

import com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.RentalOrderDBDao;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.CarForRental;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.RentalOrder;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.User;
import com.epam.rd.spring2019.jdbc_sql_liquibase.services.RentalOrderService;

import java.util.List;

public class RentalOrderServiceImpl implements RentalOrderService {

    private RentalOrderDBDao rentalOrderDBDao;

    public RentalOrderServiceImpl(RentalOrderDBDao rentalOrderDBDao) {
        this.rentalOrderDBDao = rentalOrderDBDao;
    }

    @Override
    public void createRentalOrder(User user, List<CarForRental> carForRentals) {
        RentalOrder rentalOrder = new RentalOrder(user, carForRentals);
        boolean result = rentalOrderDBDao.saveRentalOrder(rentalOrder);
        if (result) {
            System.out.println("The rental order is saved: " + rentalOrder);
        }
    }

    @Override
    public void modifyRentalOrder(long id,
                                  User userForModifyOrder,
                                  List<CarForRental> carsListForModifyOrder) {
        for (RentalOrder rentalOrder : getAllRentalOrders()) {
            long rentalOrderId = rentalOrder.getId();
            if (rentalOrderId == id) {
                rentalOrder.setUser(userForModifyOrder);
                rentalOrder.setCarForRentals(carsListForModifyOrder);
                boolean result = rentalOrderDBDao.modifyRentalOrder(id, rentalOrder);
                if (result) {
                    System.out.println("The rental order is modified: " + rentalOrder);
                }
            }
        }
    }

    @Override
    public List<RentalOrder> getAllRentalOrders() {
        return rentalOrderDBDao.getAllRentalOrders();
    }

    @Override
    public void deleteRentalOrder(long id) {
        for (RentalOrder rentalOrder : getAllRentalOrders()) {
            long rentalOrderId = rentalOrder.getId();
            if (rentalOrderId == id) {
                boolean result = rentalOrderDBDao.deleteRentalOrder(id);
                if (result) {
                    System.out.println("The rental order is deleted: " + rentalOrder);
                }
            }
        }
    }
}
