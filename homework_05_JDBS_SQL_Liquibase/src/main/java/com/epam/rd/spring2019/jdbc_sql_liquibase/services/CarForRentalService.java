package com.epam.rd.spring2019.jdbc_sql_liquibase.services;

import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.CarForRental;

import java.math.BigDecimal;
import java.util.List;

public interface CarForRentalService {


    /**
     * create a car for rental
     *
     * @param name  of the car for rental
     * @param price of the car for rental
     */
    void createCarForRental(String name, BigDecimal price);

    /**
     * get all cars for rental
     *
     * @return list of all cars for rental
     */
    List<CarForRental> getAllCarsForRental();


    /**
     * a method for modify information about car for rental
     *
     * @param id       of a car for rental
     * @param carName  of a car for rental
     * @param carPrice of a car for rental
     */
    void modifyCarForRental(long id, String carName, BigDecimal carPrice);


    /**
     * delete a car for rental
     *
     * @param id of a car for rental
     */
    void deleteCarForRental(long id);
}
