package com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl;

import com.epam.rd.spring2019.jdbc_sql_liquibase.dao.CarForRentalDao;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.CarForRental;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.DataForConnectionToH2DB.*;
import static com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.SomeGeneralServiceMethodsForDB.findCarForRental;

public class CarForRentalDBForRentalDao implements CarForRentalDao {

    public CarForRentalDBForRentalDao() {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             Statement statement = connection.createStatement()) {

            statement.execute(
                    "CREATE TABLE IF NOT EXISTS CARS(" +
                            "ID BIGINT DEFAULT 1 PRIMARY KEY AUTO_INCREMENT," +
                            " MODEL VARCHAR(20) DEFAULT NULL, PRICE DECIMAL DEFAULT 0)"
            );

        } catch (SQLException e) {
            System.out.println("SOMETHING IS GOING WRONG!!!");
        }
    }

    @Override
    public boolean saveCarForRental(CarForRental carForRental) {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "INSERT INTO CARS (MODEL, PRICE) VALUES(?, ?)")) {
            System.out.println("Saving.... Please wait");
            statement.setString(1, carForRental.getModel());
            statement.setBigDecimal(2, carForRental.getPrice());
            System.out.println("The car for rental is saved: " + "The car for rental{" +
                    "model='" + carForRental.getModel() + '\'' +
                    ", price=" + carForRental.getPrice() +
                    '}');
            statement.execute();
        } catch (SQLException e) {
            System.out.println("SOMETHING WAS GOING WRONG!!!");
        }
        return false;
    }

    @Override
    public List<CarForRental> getAllCarsForRental() {
        List<CarForRental> resultCarForRentalList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM CARS")) {
            while (resultSet.next()) {
                long id = resultSet.getLong("ID");
                String model = resultSet.getString("MODEL");
                BigDecimal price = resultSet.getBigDecimal("PRICE");
                resultCarForRentalList.add(new CarForRental(id, model, price));
            }
        } catch (SQLException e) {
            System.out.println("CARS DIDN'T FIND!!!");
        }
        return resultCarForRentalList;
    }

    @Override
    public boolean modifyCarForRental(long id, CarForRental carForRental) {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "UPDATE CARS SET MODEL = ?, PRICE = ? WHERE ID = ?")) {
            System.out.println("Modifying.... Please wait");
            statement.setLong(3, id);
            statement.setString(1, carForRental.getModel());
            statement.setBigDecimal(2, carForRental.getPrice());
            System.out.println("The car for rental is modified: " + carForRental);
            statement.execute();
        } catch (SQLException e) {
            System.out.println("SOMETHING WAS GOING WRONG!!! CAR DIDN'T FIND FOR MODIFYING!!!");
        }
        return false;
    }

    @Override
    public boolean deleteCarForRental(long carId) {
        CarForRental carForRentalForDelete = findCarForRental(carId);
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "DELETE FROM CARS WHERE ID = ?")) {
            System.out.println("Deleting... Please wait");
            statement.setLong(1, carId);
            System.out.println("The car for rental is deleted: " + carForRentalForDelete);
            statement.execute();
        } catch (SQLException e) {
            System.out.println("SOMETHING WAS GOING WRONG!!! CAR DIDN'T FIND FOR DELETING!!!");
        }
        return false;
    }

}
