package com.epam.rd.spring2019.jdbc_sql_liquibase.services.impl;

import com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.CarForRentalDBForRentalDao;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.CarForRental;
import com.epam.rd.spring2019.jdbc_sql_liquibase.services.CarForRentalService;

import java.math.BigDecimal;
import java.util.List;

public class CarForRentalServiceImpl implements CarForRentalService {

    private CarForRentalDBForRentalDao carForRentalDBDao;

    public CarForRentalServiceImpl(CarForRentalDBForRentalDao carForRentalDBDao) {
        this.carForRentalDBDao = carForRentalDBDao;
    }

    @Override
    public void createCarForRental(String model, BigDecimal price) {
        CarForRental carForRental = new CarForRental(model, price);
        boolean result = carForRentalDBDao.saveCarForRental(carForRental);
        if (result) {
            System.out.println("The car for rental is saved: " + carForRental);
        }
    }

    @Override
    public void modifyCarForRental(long id, String model, BigDecimal price) {
        for (CarForRental carForRental : getAllCarsForRental()) {
            long carForRentalId = carForRental.getId();
            if (carForRentalId == id) {
                carForRental.setModel(model);
                carForRental.setPrice(price);
                boolean result = carForRentalDBDao.modifyCarForRental(id, carForRental);
                if (result) {
                    System.out.println("The car for rental is modified: " + carForRental);
                }
            }
        }
    }

    @Override
    public List<CarForRental> getAllCarsForRental() {
        return carForRentalDBDao.getAllCarsForRental();
    }

    @Override
    public void deleteCarForRental(long id) {
        for (CarForRental carForRental : getAllCarsForRental()) {
            long carForRentalId = carForRental.getId();
            if (carForRentalId == id) {
                boolean result = carForRentalDBDao.deleteCarForRental(id);
                if (result) {
                    System.out.println("The car for rental is deleted: " + carForRental);
                }
            }
        }
    }
}
