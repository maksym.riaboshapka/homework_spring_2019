package com.epam.rd.spring2019.jdbc_sql_liquibase;

import com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.CarForRentalDBForRentalDao;
import com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.RentalOrderDBDao;
import com.epam.rd.spring2019.jdbc_sql_liquibase.dao.impl.UserDBDao;
import com.epam.rd.spring2019.jdbc_sql_liquibase.services.CarForRentalService;
import com.epam.rd.spring2019.jdbc_sql_liquibase.services.RentalOrderService;
import com.epam.rd.spring2019.jdbc_sql_liquibase.services.UserService;
import com.epam.rd.spring2019.jdbc_sql_liquibase.services.impl.CarForRentalServiceImpl;
import com.epam.rd.spring2019.jdbc_sql_liquibase.services.impl.RentalOrderServiceImpl;
import com.epam.rd.spring2019.jdbc_sql_liquibase.services.impl.UserServiceImpl;
import com.epam.rd.spring2019.jdbc_sql_liquibase.view.RentalOrderMenu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

    /*
         a start point of the application
    */
    public static void main(String[] args) throws IOException {

    /*
         configured all necessary dependencies for this application
         realizing of the dependency injection
    */
        UserDBDao userDBDao = new UserDBDao();
        CarForRentalDBForRentalDao carForRentalDBDao = new CarForRentalDBForRentalDao();
        RentalOrderDBDao rentalOrderDBDao = new RentalOrderDBDao();
        UserService userService = new UserServiceImpl(userDBDao);
        CarForRentalService carForRentalService = new CarForRentalServiceImpl(carForRentalDBDao);
        RentalOrderService rentalOrderService = new RentalOrderServiceImpl(rentalOrderDBDao);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        RentalOrderMenu rentalOrderMenu =
                new RentalOrderMenu(br, userService, carForRentalService, rentalOrderService);
        rentalOrderMenu.show();
    }

}
