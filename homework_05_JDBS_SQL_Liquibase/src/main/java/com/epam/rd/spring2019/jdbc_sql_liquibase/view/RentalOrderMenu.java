package com.epam.rd.spring2019.jdbc_sql_liquibase.view;

import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.CarForRental;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.RentalOrder;
import com.epam.rd.spring2019.jdbc_sql_liquibase.pojo.User;
import com.epam.rd.spring2019.jdbc_sql_liquibase.services.CarForRentalService;
import com.epam.rd.spring2019.jdbc_sql_liquibase.services.RentalOrderService;
import com.epam.rd.spring2019.jdbc_sql_liquibase.services.UserService;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RentalOrderMenu {

    private final BufferedReader br;
    private final UserService userService;
    private final CarForRentalService carForRentalService;
    private final RentalOrderService rentalOrderService;

    public RentalOrderMenu(BufferedReader br,
                           UserService userService,
                           CarForRentalService carForRentalService,
                           RentalOrderService rentalOrderService) {
        this.br = br;
        this.userService = userService;
        this.carForRentalService = carForRentalService;
        this.rentalOrderService = rentalOrderService;
    }

    public void show() throws IOException {

        boolean exitFromCycle = true;
        System.out.println("Create a simple database of the car rental by using items of the menu:");
        while (exitFromCycle) {
            showMenu();
            switch (br.readLine()) {
                case "1":
                    createUser();
                    break;
                case "2":
                    modifyUser();
                    break;
                case "3":
                    deleteUser();
                    break;
                case "4":
                    System.out.println("All users:");
                    showAllUsers();
                    break;
                case "5":
                    createCarForRental();
                    break;
                case "6":
                    modifyCarForRental();
                    break;
                case "7":
                    deleteCarForRental();
                    break;
                case "8":
                    System.out.println("All cars:");
                    showAllCarsForRental();
                    break;
                case "9":
                    createRentalOrder();
                    break;
                case "10":
                    modifyRentalOrder();
                    break;
                case "11":
                    deleteRentalOrder();
                    break;
                case "12":
                    showAllRentalOrders();
                    break;
                case "E":
                    exitFromCycle = false;
                    break;
                default:
                    System.out.println("wrong input!!!");
            }
        }

    }

    private void showMenu() {
        System.out.println("1. Add user");
        System.out.println("2. Modify user");
        System.out.println("3. Remove user");
        System.out.println("4. List all user");
        System.out.println();

        System.out.println("5. Add a car for rental");
        System.out.println("6. Modify the car for rental");
        System.out.println("7. Remove the car for rental");
        System.out.println("8. List of all rental's cars");
        System.out.println();

        System.out.println("9. Create a rental's order");
        System.out.println("10. Modify the rental's order");
        System.out.println("11. Remove the rental's order");
        System.out.println("12. List of all rental's orders");

        System.out.println("E. Exit");

        System.out.println("After creating and working with the database" +
                " try a command \"mvn liquibase:update\" via a command line\n" +
                " of a directory: \"homework_05_JDBC_SQL_Liquibase>mvn liquibase:update\"");
    }

    private void createUser() throws IOException {
        System.out.println("Input user's name: ");
        String name = br.readLine();
        System.out.println("Input user's surname: ");
        String surname = br.readLine();
        userService.createUser(name, surname);
    }


    private void modifyUser() throws IOException {
        showAllUsers();
        System.out.println("Input user's ID for modify: ");
        long id = readLongId();
        for (User user : userService.getAllUsers()) {
            long tempId = user.getId();
            if (tempId == id) {
                System.out.println("Input user's name: ");
                String name = br.readLine();
                System.out.println("Input user's surname: ");
                String surname = br.readLine();
                userService.modifyUser(id, name, surname);
                return;
            }
        }
    }

    private void deleteUser() {
        showAllUsers();
        System.out.println("Input user's ID to remove: ");
        long id = readLongId();
        for (User user : userService.getAllUsers()) {
            long tempId = user.getId();
            if (tempId == id) {
                userService.deleteUser(id);
                return;
            }
        }
    }

    private void showAllUsers() {
        for (User user : userService.getAllUsers()) {
            System.out.println(user);
        }
    }

    private void createCarForRental() throws IOException {
        System.out.println("Input the rental car's model: ");
        String carModel = br.readLine();
        System.out.println("Input the price of the rental car:");
        BigDecimal carPrice = readBigDecimal();
        carForRentalService.createCarForRental(carModel, carPrice);
    }

    private void modifyCarForRental() throws IOException {
        showAllCarsForRental();
        System.out.println("Input the rental car's ID to modify: ");
        long id = readLongId();
        for (CarForRental carForRental : carForRentalService.getAllCarsForRental()) {
            long tempId = carForRental.getId();
            if (tempId == id) {
                System.out.println("Input the rental car's model: ");
                String carModel = br.readLine();
                System.out.println("Input the price of the rental car:");
                BigDecimal carPrice = readBigDecimal();
                carForRentalService.modifyCarForRental(id, carModel, carPrice);
                return;
            }
        }
    }

    private void showAllCarsForRental() {
        for (CarForRental carForRental : carForRentalService.getAllCarsForRental()) {
            System.out.println(carForRental);
        }
    }

    private void deleteCarForRental() {
        showAllCarsForRental();
        System.out.println("Input the rental car's ID to remove: ");
        long id = readLongId();
        for (CarForRental carForRental : carForRentalService.getAllCarsForRental()) {
            long tempId = carForRental.getId();
            if (tempId == id) {
                carForRentalService.deleteCarForRental(id);
                return;
            }
        }
    }

    private void createRentalOrder() {
        showAllUsers();
        System.out.println("Input user's ID to create the user's rental order of the car: ");
        long userId = readLongId();
        showAllCarsForRental();
        List<CarForRental> listCarForRentals = createCarsForRentalList();
        User user = getUserById(userId);
        rentalOrderService.createRentalOrder(user, listCarForRentals);
    }

    private List<CarForRental> createCarsForRentalList() {
        List<CarForRental> listCarForRentals = new ArrayList<>();
        long rentalOrderId;
        boolean exitFromWhile = true;
        while (exitFromWhile) {
            System.out.println("Enter the rental car's ID for adding it to the rental order" +
                    " or \"-1\"-for exit)");
            rentalOrderId = readLongId();
            if (rentalOrderId != -1) {
                for (CarForRental carForRental : carForRentalService.getAllCarsForRental()) {
                    long tempCarId = carForRental.getId();
                    if (tempCarId == rentalOrderId) {
                        listCarForRentals.add(carForRental);
                    }
                }
            } else {
                exitFromWhile = false;
            }
        }
        return listCarForRentals;
    }


    private void modifyRentalOrder() {
        showAllRentalOrders();
        System.out.println("Input the rental order's ID for modify:");
        long id = readLongId();
        for (RentalOrder rentalOrder : rentalOrderService.getAllRentalOrders()) {
            long tempId = rentalOrder.getId();
            if (tempId == id) {
                User userForModifyOrder = getUserForModifyOrder();
                List<CarForRental> carsListForModifyOrder = getCarsListForModifyOrder();
                rentalOrderService.modifyRentalOrder(id, userForModifyOrder, carsListForModifyOrder);
                return;
            }
        }
    }

    private User getUserForModifyOrder() {
        User userForModifiedOrder = null;
        showAllUsers();
        System.out.println("Enter user's ID for modify:");
        long userID = readLongId();
        for (User user : userService.getAllUsers()) {
            long userTempId = user.getId();
            if (userTempId == userID) {
                userForModifiedOrder = user;
            }
        }
        return userForModifiedOrder;
    }

    private List<CarForRental> getCarsListForModifyOrder() {
        showAllCarsForRental();
        List<CarForRental> listCarForRentals = new ArrayList<>();
        long carId;
        boolean exitFromWhile = true;
        while (exitFromWhile) {
            carId = readLongId();
            if (carId != -1) {
                for (CarForRental carForRental : carForRentalService.getAllCarsForRental()) {
                    long tempCarId = carForRental.getId();
                    if (tempCarId == carId) {
                        listCarForRentals.add(carForRental);
                        return listCarForRentals;
                    }
                }
            } else {
                exitFromWhile = false;
            }
        }
        return listCarForRentals;
    }

    private void deleteRentalOrder() {
        showAllRentalOrders();
        System.out.println("Input rental order's ID for remove: ");
        long id = readLongId();
        for (RentalOrder rentalOrder : rentalOrderService.getAllRentalOrders()) {
            long tempId = rentalOrder.getId();
            if (tempId == id) {
                rentalOrderService.deleteRentalOrder(id);
                return;
            }
        }
    }

    private void showAllRentalOrders() {
        for (RentalOrder rentalOrder : rentalOrderService.getAllRentalOrders()) {
            System.out.println(rentalOrder);
        }
    }

    private long readLongId() {
        try {
            return Long.parseLong(br.readLine());
        } catch (IOException | NumberFormatException ex) {
            System.out.println("Input number please!!!");
            // recursive call
            return readLongId();
        }
    }

    private BigDecimal readBigDecimal() {
        try {
            return BigDecimal.valueOf(Long.parseLong(br.readLine()));
        } catch (IOException | NumberFormatException ex) {
            System.out.println("Input number please!!!");
            // recursive call
            return readBigDecimal();
        }
    }

    private User getUserById(long userId) {
        User user = null;
        for (User tempUser : userService.getAllUsers()) {
            long tempId = tempUser.getId();
            if (tempId == userId) {
                user = tempUser;
            }
        }
        if (user == null) {
            System.out.println("Choose \"1. Register\"");
        }
        return user;
    }

}
