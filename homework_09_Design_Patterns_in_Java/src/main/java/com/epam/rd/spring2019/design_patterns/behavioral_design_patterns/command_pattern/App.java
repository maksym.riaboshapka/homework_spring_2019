package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.command_pattern;

import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.command_pattern.button.Button;
import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.command_pattern.command.Command;
import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.command_pattern.command.impl.TurnOffEngineCommand;
import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.command_pattern.command.impl.TurnOnEngineCommand;
import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.command_pattern.engine.Engine;

public class App {

    public static void main(String[] args) {

        Engine engine = new Engine();
        Command commandTurnOn = new TurnOnEngineCommand(engine);
        Command commandTurnOff = new TurnOffEngineCommand(engine);

        Button button = new Button(commandTurnOn, commandTurnOff);
        button.pressButton();
        button.againPressButton();

    }

}
