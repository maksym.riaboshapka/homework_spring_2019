package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.template_method_pattern;

import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.template_method_pattern.documents.Document;
import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.template_method_pattern.documents.typesOfDocuments.Doc;
import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.template_method_pattern.documents.typesOfDocuments.DocumentsTypes;
import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.template_method_pattern.documents.typesOfDocuments.Pdf;
import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.template_method_pattern.documents.typesOfDocuments.Txt;

public class AppTemplateMethod {

    public static void main(String[] args) {

        final DocumentsTypes documentsTypes = DocumentsTypes.PDF;
        Document document;
        switch (documentsTypes) {
            case TXT:
                document = new Txt();
                break;
            case DOC:
                document = new Doc();
                break;
            case PDF:
                document = new Pdf();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + documentsTypes);
        }
        document.workWithDocument();

    }

}

