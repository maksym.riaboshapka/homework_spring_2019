package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.command_pattern.command;

public interface Command {

    /**
     * do something
     */
    void doIt();

}
