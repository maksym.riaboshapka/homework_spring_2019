package com.epam.rd.spring2019.design_patterns.structural_design_patterns.decorator_pattern.log_in;

public interface LogIn {

    /**
     * to log in into account
     */
    void loginIn();

}

