package com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.creating;

import com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.creator.CarCreator;
import com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.creator.impl.DieselEngineCarCreator;
import com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.creator.impl.PetrolEngineCarCreator;
import com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.product.Car;

public class Creating {

    private CarCreator carCreator;

    public Creating(String typoOfEngine) {

        switch (typoOfEngine) {
            case "petrol":
                carCreator = new PetrolEngineCarCreator();
                break;
            case "diesel":
                carCreator = new DieselEngineCarCreator();
                break;
            default:
                System.out.println("Chose a type of engine, please.");
        }

    }

    public Car buildCar() {
        return carCreator.createCar();
    }

}
