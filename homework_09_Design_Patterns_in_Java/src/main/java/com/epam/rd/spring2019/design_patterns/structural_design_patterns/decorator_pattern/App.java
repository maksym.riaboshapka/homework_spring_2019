package com.epam.rd.spring2019.design_patterns.structural_design_patterns.decorator_pattern;

import com.epam.rd.spring2019.design_patterns.structural_design_patterns.decorator_pattern.decorator.FbAccountLogIn;
import com.epam.rd.spring2019.design_patterns.structural_design_patterns.decorator_pattern.decorator.GoogleAccountLogIn;
import com.epam.rd.spring2019.design_patterns.structural_design_patterns.decorator_pattern.decorator.LogInDecorator;
import com.epam.rd.spring2019.design_patterns.structural_design_patterns.decorator_pattern.log_in.impl.LogInImpl;

public class App {

    public static void main(String[] args) {
        LogInDecorator loginDecorator = new GoogleAccountLogIn(new LogInImpl());
        loginDecorator.loginIn();
        loginDecorator.newLoginInWith();

        System.out.println();

        loginDecorator = new FbAccountLogIn(new LogInImpl());
        loginDecorator.loginIn();
        loginDecorator.newLoginInWith();

    }

}

