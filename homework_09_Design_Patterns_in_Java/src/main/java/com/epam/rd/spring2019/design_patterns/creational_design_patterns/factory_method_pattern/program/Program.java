package com.epam.rd.spring2019.design_patterns.creational_design_patterns.factory_method_pattern.program;

public interface Program {

    /**
     * get a version of the program for downloading, depending on the OS
     */
    void getProgram();

}

