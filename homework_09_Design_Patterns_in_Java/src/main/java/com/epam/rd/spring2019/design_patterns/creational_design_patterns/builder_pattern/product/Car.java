package com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.product;

import com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.constituents.engines.Engine;

public class Car {

    public void buildBase() {
        print("\nCreating a base");
    }

    public void buildWheels() {
        print("\nFixing the wheel");
    }

    public void buildEngine(Engine engine) {
        print("\nPutting the engine: " + engine.getEngineType());
    }

    private void print(String msg) {
        System.out.println(msg);
    }

}

