package com.epam.rd.spring2019.design_patterns.structural_design_patterns.decorator_pattern.log_in.impl;

import com.epam.rd.spring2019.design_patterns.structural_design_patterns.decorator_pattern.log_in.LogIn;

public class LogInImpl implements LogIn {

    @Override
    public void loginIn() {
        System.out.println("You successful login");
    }

}

