package com.epam.rd.spring2019.design_patterns.structural_design_patterns.decorator_pattern.decorator;

import com.epam.rd.spring2019.design_patterns.structural_design_patterns.decorator_pattern.log_in.LogIn;

public abstract class LogInDecorator implements LogIn {
    protected LogIn login;

    public LogInDecorator(LogIn login) {
        this.login = login;
    }

    @Override
    public void loginIn() {
        login.loginIn();
    }

    public void newLoginInWith() {
        System.out.println("Do Nothing");
    }

}

