package com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern;

import com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.creating.Creating;

public class App {

    private static final String PETROL_ENGINE = "petrol";
    private static final String DIESEL_ENGINE = "diesel";

    public static void main(String[] args) {
        Creating creating = new Creating(PETROL_ENGINE);
        creating.buildCar();

        System.out.println();

        creating = new Creating(DIESEL_ENGINE);
        creating.buildCar();
    }

}

