package com.epam.rd.spring2019.design_patterns.creational_design_patterns.factory_method_pattern;

import com.epam.rd.spring2019.design_patterns.creational_design_patterns.factory_method_pattern.factory.Factory;
import com.epam.rd.spring2019.design_patterns.creational_design_patterns.factory_method_pattern.program.Program;

public class App {

    public static void main(String[] args) {

        String inputOS = "windows";
        Factory factory = new Factory();
        Program program = factory.getProgramForCurrentVersionOS(inputOS);
        program.getProgram();

        inputOS = "linux";
        program = factory.getProgramForCurrentVersionOS(inputOS);
        program.getProgram();

        inputOS = "mac";
        program = factory.getProgramForCurrentVersionOS(inputOS);
        program.getProgram();

    }

}

