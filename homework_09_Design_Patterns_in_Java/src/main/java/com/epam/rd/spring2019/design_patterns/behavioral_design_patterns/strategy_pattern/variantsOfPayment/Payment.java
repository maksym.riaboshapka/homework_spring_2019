package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.strategy_pattern.variantsOfPayment;

public interface Payment {

    /**
     * a realization of the variant of payment
     */
    void payment();

}

