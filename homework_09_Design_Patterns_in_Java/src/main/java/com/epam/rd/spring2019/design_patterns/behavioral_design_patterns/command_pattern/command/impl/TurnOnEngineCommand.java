package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.command_pattern.command.impl;

import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.command_pattern.command.Command;
import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.command_pattern.engine.Engine;

public class TurnOnEngineCommand implements Command {

    private Engine engine;

    public TurnOnEngineCommand(Engine engine) {
        this.engine = engine;
    }

    @Override
    public void doIt() {
        engine.turnOn();
    }
}

