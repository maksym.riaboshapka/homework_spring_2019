package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.strategy_pattern;

import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.strategy_pattern.strategyContext.ContextPayment;
import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.strategy_pattern.variantsOfPayment.Payment;
import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.strategy_pattern.variantsOfPayment.impl.CardPayment;
import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.strategy_pattern.variantsOfPayment.impl.CashPayment;
import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.strategy_pattern.variantsOfPayment.impl.PayPalPayment;

public class App {

    public static void main(String[] args) {

        Payment cashPayment = new CashPayment();
        ContextPayment cashContextPayment = new ContextPayment(cashPayment);
        cashContextPayment.contextPaymentStrategy();

        Payment cardPayment = new CardPayment();
        ContextPayment cardContextPayment = new ContextPayment(cardPayment);
        cardContextPayment.contextPaymentStrategy();

        Payment payPalPayment = new PayPalPayment();
        ContextPayment electronicContextPayment = new ContextPayment(payPalPayment);
        electronicContextPayment.contextPaymentStrategy();

    }

}
