package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.command_pattern.engine;

public class Engine {

    public void turnOn() {
        System.out.println("\nThe engine is on");
    }

    public void turnOff() {
        System.out.println("\nThe engine is off");
    }

}
