package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.template_method_pattern.documents.typesOfDocuments;

import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.template_method_pattern.documents.Document;

public class Doc extends Document {

    private static final String DOCUMENT_DOC = "\nDocument.doc is: ";

    @Override
    protected void openDocument() {
        System.out.println(DOCUMENT_DOC + "opened");
    }

    @Override
    protected void readDocument() {
        System.out.println(DOCUMENT_DOC + "read");
    }

    @Override
    protected void writeDocument() {
        System.out.println(DOCUMENT_DOC + "written");
    }

    @Override
    protected void closeDocument() {
        System.out.println(DOCUMENT_DOC + "closed");
    }

}
