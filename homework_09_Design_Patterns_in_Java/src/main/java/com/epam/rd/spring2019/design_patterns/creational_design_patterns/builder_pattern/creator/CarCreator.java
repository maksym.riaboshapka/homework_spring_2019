package com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.creator;

import com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.product.Car;

public abstract class CarCreator {

    protected Car car;

    public abstract Car createCar();

}

