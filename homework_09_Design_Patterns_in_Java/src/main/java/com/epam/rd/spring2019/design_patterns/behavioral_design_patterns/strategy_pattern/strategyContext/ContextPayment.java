package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.strategy_pattern.strategyContext;

import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.strategy_pattern.variantsOfPayment.Payment;

public class ContextPayment {

    private Payment payment;

    public ContextPayment(Payment payment) {
        this.payment = payment;
    }

    public void contextPaymentStrategy() {
        payment.payment();
    }

}

