package com.epam.rd.spring2019.design_patterns.creational_design_patterns.factory_method_pattern.factory;

import com.epam.rd.spring2019.design_patterns.creational_design_patterns.factory_method_pattern.program.Program;
import com.epam.rd.spring2019.design_patterns.creational_design_patterns.factory_method_pattern.program.impl.ProgramForLinuxOSImpl;
import com.epam.rd.spring2019.design_patterns.creational_design_patterns.factory_method_pattern.program.impl.ProgramForMacOSImpl;
import com.epam.rd.spring2019.design_patterns.creational_design_patterns.factory_method_pattern.program.impl.ProgramForWindowsOSImpl;

public class Factory {

    public Program getProgramForCurrentVersionOS(String inputOS) {

        Program programForCurrentVersionOS = null;

        switch (inputOS) {
            case "windows":
                programForCurrentVersionOS = new ProgramForWindowsOSImpl();
                break;
            case "linux":
                programForCurrentVersionOS = new ProgramForLinuxOSImpl();
                break;
            case "mac":
                programForCurrentVersionOS = new ProgramForMacOSImpl();
                break;
            default:
                System.out.println("Wrong OS type passed!");
        }
        return programForCurrentVersionOS;
    }

}

