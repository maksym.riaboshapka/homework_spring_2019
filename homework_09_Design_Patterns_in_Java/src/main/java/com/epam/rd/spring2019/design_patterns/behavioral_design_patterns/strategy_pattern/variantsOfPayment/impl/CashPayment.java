package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.strategy_pattern.variantsOfPayment.impl;

import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.strategy_pattern.variantsOfPayment.Payment;

public class CashPayment implements Payment {

    @Override
    public void payment() {
        System.out.println("\nCash is used for payment");
    }

}
