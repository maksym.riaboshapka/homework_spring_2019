package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.template_method_pattern.documents;

public abstract class Document {

    protected abstract void openDocument();

    protected abstract void readDocument();

    protected abstract void writeDocument();

    protected abstract void closeDocument();

    public final void workWithDocument() {
        openDocument();
        readDocument();
        writeDocument();
        closeDocument();
    }

}
