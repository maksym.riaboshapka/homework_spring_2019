package com.epam.rd.spring2019.design_patterns.structural_design_patterns.decorator_pattern.decorator;

import com.epam.rd.spring2019.design_patterns.structural_design_patterns.decorator_pattern.log_in.LogIn;

public class FbAccountLogIn extends LogInDecorator {

    public FbAccountLogIn(LogIn login) {
        super(login);
    }

    @Override
    public void loginIn() {
        super.loginIn();
    }

    @Override
    public void newLoginInWith() {
        System.out.println("LogIn with FB is successful");
    }

}

