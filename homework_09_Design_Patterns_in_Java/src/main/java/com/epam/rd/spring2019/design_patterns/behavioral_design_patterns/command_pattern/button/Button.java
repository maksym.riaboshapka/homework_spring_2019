package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.command_pattern.button;

import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.command_pattern.command.Command;

public class Button {

    private Command pressButtonCommand;
    private Command againPressButtonCommand;

    public Button(Command pressButtonCommand, Command againPressButtonCommand) {
        this.pressButtonCommand = pressButtonCommand;
        this.againPressButtonCommand = againPressButtonCommand;
    }

    public void pressButton() {
        pressButtonCommand.doIt();
    }

    public void againPressButton() {
        againPressButtonCommand.doIt();
    }

}
