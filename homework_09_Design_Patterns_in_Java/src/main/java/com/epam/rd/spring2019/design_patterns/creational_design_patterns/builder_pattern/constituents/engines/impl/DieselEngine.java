package com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.constituents.engines.impl;

import com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.constituents.engines.Engine;

public class DieselEngine implements Engine {

    @Override
    public String getEngineType() {
        return "a diesel engine";
    }

}

