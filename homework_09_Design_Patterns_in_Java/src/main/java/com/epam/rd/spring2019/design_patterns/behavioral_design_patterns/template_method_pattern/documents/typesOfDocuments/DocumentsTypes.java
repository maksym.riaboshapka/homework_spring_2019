package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.template_method_pattern.documents.typesOfDocuments;

public enum DocumentsTypes {

    TXT, DOC, PDF

}

