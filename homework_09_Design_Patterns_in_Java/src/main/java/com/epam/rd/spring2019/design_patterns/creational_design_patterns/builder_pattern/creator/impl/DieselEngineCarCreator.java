package com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.creator.impl;

import com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.constituents.engines.Engine;
import com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.constituents.engines.impl.DieselEngine;
import com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.creator.CarCreator;
import com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.product.Car;

public class DieselEngineCarCreator extends CarCreator {

    public DieselEngineCarCreator() {
        car = new Car();
    }

    @Override
    public Car createCar() {
        car.buildBase();
        car.buildWheels();
        Engine engine = new DieselEngine();
        car.buildEngine(engine);
        return car;
    }
}

