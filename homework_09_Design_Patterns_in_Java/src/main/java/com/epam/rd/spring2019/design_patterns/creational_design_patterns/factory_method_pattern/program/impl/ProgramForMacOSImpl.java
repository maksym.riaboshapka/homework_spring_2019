package com.epam.rd.spring2019.design_patterns.creational_design_patterns.factory_method_pattern.program.impl;

import com.epam.rd.spring2019.design_patterns.creational_design_patterns.factory_method_pattern.program.Program;

public class ProgramForMacOSImpl implements Program {
    @Override
    public void getProgram() {
        System.out.println("\nApply the Program for Mac OS");
    }
}

