package com.epam.rd.spring2019.design_patterns.creational_design_patterns.builder_pattern.constituents.engines;

public interface Engine {

    /**
     * a type of engine
     *
     * @return a type of engine
     */
    String getEngineType();

}

