package com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.template_method_pattern.documents.typesOfDocuments;

import com.epam.rd.spring2019.design_patterns.behavioral_design_patterns.template_method_pattern.documents.Document;

public class Txt extends Document {

    private static final String DOCUMENT_TXT = "\nDocument.txt is: ";

    @Override
    protected void openDocument() {
        System.out.println(DOCUMENT_TXT + "opened");
    }

    @Override
    protected void readDocument() {
        System.out.println(DOCUMENT_TXT + "read");
    }

    @Override
    protected void writeDocument() {
        System.out.println(DOCUMENT_TXT + "written");
    }

    @Override
    protected void closeDocument() {
        System.out.println(DOCUMENT_TXT + "closed");
    }

}

