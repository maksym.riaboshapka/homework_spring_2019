# There are 6 implementations of design patterns in this branch of the current homework.

* 1. "Factory Method" - the creational design pattern
>   Design pattern "Factory Method" can be used when it is necessary to choose
>   which version of the program to choose to download, depending on the OS.

* 2. "Builder" - the creational design pattern
>   Design pattern "Builder" can be used for creating a new something, for example,
>   creating a custom variant a car via an official car manufacturer's site.

* 3. "Decorator" - the structural design pattern
>   Design pattern "Decorator" can be used to implement an idea of dynamically adding
>   different ways to log in into account through different logging systems via
>   adding new functionality to an existing object without binding its structure.

* 4. "Template Method" - the behavioral design pattern
>   Design pattern "Template Method" can be used to implement the idea of using
>   a common algorithm to work with different file formats.

* 5. "Strategy" - the behavioral design pattern
>   Design pattern "Strategy" can implement the idea of different ways
>   to make payments using similar algorithms.

* 6. "Command" - the behavioral design pattern
>   Design pattern "Command" can implement the idea of the functional purpose
>   of various buttons for managing the car.