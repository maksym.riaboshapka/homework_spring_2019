<%@ page import="com.epam.rd.spring2019.petproject.domain.Car" %>
<%@ page import="com.epam.rd.spring2019.petproject.domain.Car" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Delete the car</title>
    <link rel="stylesheet" href="styles/w3.css">
</head>
<body class="w3-light-grey">
<div class="w3-container w3-blue-grey w3-opacity w3-right-align">
    <h1>Education web-application-pet-project!)</h1>
</div>


<div class="w3-container w3-padding">
    <%
        if (request.getAttribute("car") != null) {
            Car car = (Car) request.getAttribute("car");
            out.println("<div class=\"w3-panel w3-lime w3-display-container w3-card-4 w3-round\">\n" +
                    "   <span onclick=\"this.parentElement.style.display='none'\"\n" +
                    "   class=\"w3-button w3-margin-right w3-display-right w3-round-large" +
                    " w3-hover-lime w3-border w3-border-lime w3-hover-border-grey\">×</span>\n" +
                    "<h5>Car {" +
                    "name='" + car.getName() + '\'' +
                    ", price='" + car.getPrice() + '\'' +
                    "} deleted!</h5>\n" + "</div>");
        }
    %>
    <div class="w3-card-4">
        <div class="w3-container w3-center w3-lime">
            <h2 class="w3-text-pink">Delete an existing car!!!</h2>
            <h2 class="w3-text-black">Enter car's ID to delete the car</h2>
        </div>

        <form method="post" class="w3-selection w3-light-grey w3-padding">
            <label>Car's ID:
                <input type="text" name="id"
                       class="w3-input w3-animate-input w3-border w3-round-large" style="width: 30%"><br/>
            </label>

            <button type="submit" class="w3-btn w3-orange w3-round-large w3-margin-bottom">Submit!</button>
        </form>
    </div>
</div>

<div class="w3-container w3-grey w3-opacity w3-right-align w3-padding">
    <button class="w3-btn w3-round-large" onclick="location.href='/carMenu.html'">Back to car menu</button>

    <button class="w3-btn w3-round-large" onclick="location.href='/'">Back to main</button>
</div>
</body>
</html>
