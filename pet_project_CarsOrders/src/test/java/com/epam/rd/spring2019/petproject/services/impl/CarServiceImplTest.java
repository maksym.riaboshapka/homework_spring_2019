package com.epam.rd.spring2019.petproject.services.impl;

import com.epam.rd.spring2019.petproject.dao.impl.CarDBDao;
import com.epam.rd.spring2019.petproject.domain.Car;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CarServiceImplTest {

    private CarDBDao productDBDaoForMock = mock(CarDBDao.class);
    private CarServiceImpl productServiceForTest;
    private long id;
    private String name;
    private BigDecimal price;
    private Car carForMock;
    private Car carForList;
    private boolean expectedBooleanResult;

    @Before
    public void setUp() throws Exception {
        productServiceForTest = new CarServiceImpl(productDBDaoForMock);
        id = 0L;
        name = "Tesla";
        price = BigDecimal.valueOf(20);
        carForMock = new Car(name, price);
        carForList = new Car(name, price);
        expectedBooleanResult = true;
    }

    @After
    public void tearDown() throws Exception {
        productServiceForTest = null;
        id = 0L;
        name = "";
        price = BigDecimal.valueOf(0);
        carForMock = null;
        expectedBooleanResult = false;

    }

    @Test
    public void createProduct() {
        //GIVEN
        when(productDBDaoForMock.saveCar(carForMock)).thenReturn(expectedBooleanResult);
        //WHEN
        productServiceForTest.createCar(name, price);
        //THEN
        verify(productDBDaoForMock, times(1)).saveCar(carForMock);
    }

    @Test
    public void modifyProduct() {
        //GIVEN
        when(productDBDaoForMock.modifyCar(id, carForMock)).thenReturn(expectedBooleanResult);
        //WHEN
        productServiceForTest.modifyCar(id, name, price);
        //THEN
        verify(productDBDaoForMock, times(1)).getAllCars();
    }

    @Test
    public void getAllProducts() {
        //GIVEN
        List<Car> expectedProductsList = new ArrayList<>();
        expectedProductsList.add(carForList);
        List<Car> productsListForMock = new ArrayList<>();
        productsListForMock.add(carForMock);
        when(productDBDaoForMock.getAllCars()).thenReturn(productsListForMock);
        //WHEN
        List<Car> productsList = productServiceForTest.getAllCars();
        //THEN
        assertEquals(expectedProductsList, productsList);
        verify(productDBDaoForMock, times(1)).getAllCars();
    }

    @Test
    public void delete() {
        //GIVEN
        when(productDBDaoForMock.deleteCar(id)).thenReturn(expectedBooleanResult);
        //WHEN
        productServiceForTest.deleteCar(id);
        //THEN
        verify(productDBDaoForMock, times(1)).getAllCars();
    }
}