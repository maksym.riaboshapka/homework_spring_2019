package com.epam.rd.spring2019.petproject.dao.impl;

import com.epam.rd.spring2019.petproject.domain.Car;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class CarDBDaoTest {

    private static final String DB_URL = "jdbc:h2:tcp://localhost/~/ExampleShop";
    private static final String LOGIN = "testForTest";
    private static final String PASSWORD = "testForTest";


    private Connection connection = mock(Connection.class);
    private CarDBDao productDBDao = mock(CarDBDao.class);
    private long id;
    private String name;
    private BigDecimal price;
    private boolean expectedBooleanResult;


    @Before
    public void setUp() throws Exception {
        id = 0L;
        name = "apple";
        price = BigDecimal.valueOf(20);
        expectedBooleanResult = true;

    }

    @After
    public void tearDown() throws Exception {
        id = 0L;
        name = "";
        price = BigDecimal.valueOf(0);
        expectedBooleanResult = false;

    }

    @Test(expected = SQLException.class)
    public void saveProduct() throws SQLException {
        //GIVEN
        connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
        Car carForSave = new Car(name, price);
        //WHEN
        boolean productSavingResult = productDBDao.saveCar(carForSave);
        //THEN
        assertEquals(expectedBooleanResult, productSavingResult);
        connection.close();
    }

    @Test(expected = SQLException.class)
    public void modifyProduct() throws SQLException {
        //GIVEN
        connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
        Car carForModify = new Car(name, price);
        //WHEN
        boolean productModifyingResult = productDBDao.modifyCar(id, carForModify);
        //THEN
        assertEquals(expectedBooleanResult, productModifyingResult);
        connection.close();
    }

    @Test(expected = SQLException.class)
    public void getAllProducts() throws SQLException {
        //GIVEN
        connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
        Car carForList = new Car(name, price);
        List<Car> expectedProductsList = new ArrayList<>();
        expectedProductsList.add(carForList);
        //WHEN
        List<Car> allCars = productDBDao.getAllCars();
        //THEN
        assertEquals(expectedProductsList, allCars);
        connection.close();
    }

    @Test(expected = SQLException.class)
    public void deleteProduct() throws SQLException {
        //GIVEN
        connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
        //WHEN
        boolean productDeletingResult = productDBDao.deleteCar(id);
        //THEN
        assertEquals(expectedBooleanResult, productDeletingResult);
        connection.close();
    }
}