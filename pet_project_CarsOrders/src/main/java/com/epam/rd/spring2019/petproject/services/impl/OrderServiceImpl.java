package com.epam.rd.spring2019.petproject.services.impl;

import com.epam.rd.spring2019.petproject.dao.impl.OrderDBDao;
import com.epam.rd.spring2019.petproject.domain.Client;
import com.epam.rd.spring2019.petproject.domain.Order;
import com.epam.rd.spring2019.petproject.domain.Car;
import com.epam.rd.spring2019.petproject.services.OrderService;

import java.util.List;

public class OrderServiceImpl implements OrderService {

    private OrderDBDao orderDBDao;

    public OrderServiceImpl(OrderDBDao orderDBDao) {
        this.orderDBDao = orderDBDao;
    }

    @Override
    public void createOrder(Client client, List<Car> cars) {
        Order order = new Order(client, cars);
        boolean result = orderDBDao.saveOrder(order);
        if (result) {
            System.out.println("Order Saved: " + order);
        }
    }

    @Override
    public void modifyOrder(long id, Client clientForModifyOrder, List<Car> productsListForModifyOrder) {
        for (Order order : getAllOrders()) {
            long orderId = order.getId();
            if (orderId == id) {
                order.setClient(clientForModifyOrder);
                order.setCars(productsListForModifyOrder);
                boolean result = orderDBDao.modifyOrder(id, order);
                if (result) {
                    System.out.println("Order Modified: " + order);
                }
            }
        }
    }

    @Override
    public List<Order> getAllOrders() {
        return orderDBDao.getAllOrders();
    }

    @Override
    public void delete(long id) {
        for (Order order : getAllOrders()) {
            long orderId = order.getId();
            if (orderId == id) {
                boolean result = orderDBDao.deleteOrder(id);
                if (result) {
                    System.out.println("Order Deleted: " + order);
                }
            }
        }
    }
}
