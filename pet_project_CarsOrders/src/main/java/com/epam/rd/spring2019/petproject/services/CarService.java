package com.epam.rd.spring2019.petproject.services;

import com.epam.rd.spring2019.petproject.domain.Car;

import java.math.BigDecimal;
import java.util.List;

public interface CarService {


    /**
     * create product
     *
     * @param name  of product
     * @param price of product
     */
    void createCar(String name, BigDecimal price);

    /**
     * get all products
     *
     * @return list of all products
     */
    List<Car> getAllCars();


    /**
     * a method for the admin which modify information about product
     *
     * @param id           of product
     * @param productName  of product
     * @param productPrice of product
     */
    void modifyCar(long id, String productName, BigDecimal productPrice);


    /**
     * deleteCar product
     *
     * @param id of product
     */
    void deleteCar(long id);
}
