package com.epam.rd.spring2019.petproject.servlets.ordersServlets;

import com.epam.rd.spring2019.petproject.dao.impl.OrderDBDao;
import com.epam.rd.spring2019.petproject.domain.Order;
import com.epam.rd.spring2019.petproject.services.OrderService;
import com.epam.rd.spring2019.petproject.services.impl.OrderServiceImpl;
import com.epam.rd.spring2019.petproject.validators.ValidationService;
import com.epam.rd.spring2019.petproject.validators.impl.ValidationServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.rd.spring2019.petproject.servlets.viewsPathForServlets.ViewsPathForServlets.*;

public class DeleteOrderServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher(DELETE_ORDER_JSP);
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String ordersID = req.getParameter("ordersID");

        // check out if data from a browser are "empty"
        if (!ordersID.isEmpty()) {
            OrderDBDao orderDBDao = new OrderDBDao();
            OrderService orderService = new OrderServiceImpl(orderDBDao);
            Order orderForDelete = orderDBDao.findOrder(Long.parseLong(ordersID));
            // deleteCar an existing order
            orderService.delete(Long.parseLong(ordersID));
            req.setAttribute("order", orderForDelete);
            doGet(req, resp);
        } else {
            doGet(req, resp);
        }
    }

}
