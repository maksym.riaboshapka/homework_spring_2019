package com.epam.rd.spring2019.petproject.servlets.clientsServlets;

import com.epam.rd.spring2019.petproject.dao.impl.ClientDBDao;
import com.epam.rd.spring2019.petproject.domain.Client;
import com.epam.rd.spring2019.petproject.services.ClientService;
import com.epam.rd.spring2019.petproject.services.impl.ClientServiceImpl;
import com.epam.rd.spring2019.petproject.validators.ValidationService;
import com.epam.rd.spring2019.petproject.validators.impl.ValidationServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.rd.spring2019.petproject.servlets.viewsPathForServlets.ViewsPathForServlets.*;

public class ClientsListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ClientDBDao clientDBDao = new ClientDBDao();
        ValidationService validationService = new ValidationServiceImpl();
        ClientService clientService = new ClientServiceImpl(clientDBDao, validationService);
        List<Client> clientsList = clientService.getAllClients();
        req.setAttribute("clientsList", clientsList);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher(CLIENTS_LIST_JSP);
        requestDispatcher.forward(req, resp);
    }

}
