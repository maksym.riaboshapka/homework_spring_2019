package com.epam.rd.spring2019.petproject.dao;

import com.epam.rd.spring2019.petproject.domain.Order;

import java.util.List;

public interface OrderDao {

    /**
     * save order
     *
     * @param order for save
     * @return true is order is saved
     */
    boolean saveOrder(Order order);


    /**
     * to read a list of all orders
     *
     * @return list of all products
     */
    List<Order> getAllOrders();


    /**
     * modify order
     *
     * @param id    of order for save
     * @param order of order for save
     * @return true if order is modified
     */
    boolean modifyOrder(long id, Order order);


    /**
     * deleteCar order by Id
     *
     * @param orderId of order
     * @return true if order was deleted
     */
    boolean deleteOrder(long orderId);
}
