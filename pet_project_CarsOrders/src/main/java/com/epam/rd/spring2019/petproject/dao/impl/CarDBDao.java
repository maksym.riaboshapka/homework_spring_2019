package com.epam.rd.spring2019.petproject.dao.impl;

import com.epam.rd.spring2019.petproject.dao.CarDao;
import com.epam.rd.spring2019.petproject.domain.Car;

import static com.epam.rd.spring2019.petproject.dao.impl.DataForConnectionToH2DB.*;


import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarDBDao implements CarDao {

    public CarDBDao() {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             Statement statement = connection.createStatement()) {

            statement.execute(
                    "CREATE TABLE IF NOT EXISTS CARS(ID BIGINT DEFAULT 1 PRIMARY KEY AUTO_INCREMENT," +
                            " NAME VARCHAR(20) DEFAULT NULL, PRICE DECIMAL DEFAULT 0)"
            );

        } catch (SQLException e) {
            System.out.println("SOMETHING IS GOING WRONG!!!");
        }
    }

    @Override
    public boolean saveCar(Car car) {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "INSERT INTO CARS (NAME, PRICE) VALUES(?, ?)")) {
            System.out.println("Saving.... Please wait");
            statement.setString(1, car.getName());
            statement.setBigDecimal(2, car.getPrice());
            System.out.println("Car Saved: " + "Car{" +
                    "name='" + car.getName() + '\'' +
                    ", price=" + car.getPrice() +
                    '}');
            statement.execute();
        } catch (SQLException e) {
            System.out.println("SOMETHING WAS GOING WRONG!!!");
        }
        return false;
    }

    @Override
    public List<Car> getAllCars() {
        List<Car> resultCarList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM CARS")) {
            while (resultSet.next()) {
                long id = resultSet.getLong("ID");
                String name = resultSet.getString("NAME");
                BigDecimal price = resultSet.getBigDecimal("PRICE");
                resultCarList.add(new Car(id, name, price));
            }
        } catch (SQLException e) {
            System.out.println("CARS DIDN'T FIND!!!");
        }
        return resultCarList;
    }

    @Override
    public boolean modifyCar(long id, Car car) {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "UPDATE CARS SET NAME = ?, PRICE = ? WHERE ID = ?")) {
            System.out.println("Modifying.... Please wait");
            statement.setLong(3, id);
            statement.setString(1, car.getName());
            statement.setBigDecimal(2, car.getPrice());
            System.out.println("Car Modified: " + car);
            statement.execute();
        } catch (SQLException e) {
            System.out.println("SOMETHING WAS GOING WRONG!!! CAR DIDN'T FIND FOR MODIFYING!!!");
        }
        return false;
    }

    @Override
    public boolean deleteCar(long carId) {
        Car carForDelete = findCar(carId);
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "DELETE FROM CARS WHERE ID = ?")) {
            System.out.println("Deleting... Please wait");
            statement.setLong(1, carId);
            System.out.println("Car Deleted: " + carForDelete);
            statement.execute();
        } catch (SQLException e) {
            System.out.println("SOMETHING WAS GOING WRONG!!! CAR DIDN'T FIND FOR DELETING!!!");
        }
        return false;
    }

    public Car findCar(long carId) {
        try (Connection connection = DriverManager.getConnection(DB_URL, LOGIN, PASSWORD);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM CARS WHERE ID = ?")) {
            statement.setLong(1, carId);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            long id = resultSet.getLong("ID");
            String name = resultSet.getString("NAME");
            BigDecimal price = resultSet.getBigDecimal("PRICE");
            resultSet.close();
            return new Car(id, name, price);
        } catch (SQLException e) {
            System.out.println("CAR DIDN'T FIND!!!");
        }
        return null;
    }
}
