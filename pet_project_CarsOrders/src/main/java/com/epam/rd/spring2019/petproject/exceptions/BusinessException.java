package com.epam.rd.spring2019.petproject.exceptions;

public class BusinessException extends Exception {

    public BusinessException(String msg) {
        super(msg);
    }

}