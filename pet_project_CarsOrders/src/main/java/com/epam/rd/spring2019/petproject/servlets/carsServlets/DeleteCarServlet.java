package com.epam.rd.spring2019.petproject.servlets.carsServlets;

import com.epam.rd.spring2019.petproject.dao.impl.CarDBDao;
import com.epam.rd.spring2019.petproject.domain.Car;
import com.epam.rd.spring2019.petproject.services.CarService;
import com.epam.rd.spring2019.petproject.services.impl.CarServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.rd.spring2019.petproject.servlets.viewsPathForServlets.ViewsPathForServlets.*;

public class DeleteCarServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher(DELETE_CAR_JSP);
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String carsID = req.getParameter("id");

        // check out if data from a browser are "empty"
        if (!carsID.isEmpty()) {
            CarDBDao carDBDao = new CarDBDao();
            CarService carService = new CarServiceImpl(carDBDao);
            Car carForDelete = carDBDao.findCar(Long.parseLong(carsID));
            // deleteCar an existing product
            carService.deleteCar(Long.parseLong(carsID));
            req.setAttribute("product", carForDelete);
            doGet(req, resp);
        } else {
            doGet(req, resp);
        }
    }


}
