package com.epam.rd.spring2019.petproject.servlets.viewsPathForServlets;

public class ViewsPathForServlets {

    // paths of client's jsp-pages
    public static final String CLIENTS_LIST_JSP = "views/clientsViews/clientsList.jsp";
    public static final String CREATE_CLIENT_JSP = "views/clientsViews/createClient.jsp";
    public static final String DELETE_CLIENT_JSP = "views/clientsViews/deleteClient.jsp";
    public static final String MODIFY_CLIENT_JSP = "views/clientsViews/modifyClient.jsp";

    // paths of product's jsp-pages
    public static final String CARS_LIST_JSP = "views/carsViews/carsList.jsp";
    public static final String CREATE_CAR_JSP = "views/carsViews/createCar.jsp";
    public static final String DELETE_CAR_JSP = "views/carsViews/deleteCar.jsp";
    public static final String MODIFY_CAR_JSP = "views/carsViews/modifyCar.jsp";

    // paths of order's jsp-pages
    public static final String ORDERS_LIST_JSP = "views/ordersViews/ordersList.jsp";
    public static final String CREATE_ORDER_JSP = "views/ordersViews/createOrder.jsp";
    public static final String DELETE_ORDER_JSP = "views/ordersViews/deleteOrder.jsp";
    public static final String MODIFY_ORDER_JSP = "views/ordersViews/modifyOrder.jsp";

}
