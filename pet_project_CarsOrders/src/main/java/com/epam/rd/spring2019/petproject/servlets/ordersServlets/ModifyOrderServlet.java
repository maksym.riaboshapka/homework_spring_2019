package com.epam.rd.spring2019.petproject.servlets.ordersServlets;

import com.epam.rd.spring2019.petproject.dao.impl.OrderDBDao;
import com.epam.rd.spring2019.petproject.domain.Car;
import com.epam.rd.spring2019.petproject.domain.Client;
import com.epam.rd.spring2019.petproject.domain.Order;
import com.epam.rd.spring2019.petproject.domain.Car;
import com.epam.rd.spring2019.petproject.services.OrderService;
import com.epam.rd.spring2019.petproject.services.impl.OrderServiceImpl;
import com.epam.rd.spring2019.petproject.servlets.iDForChecking.IDForChecking;
import com.epam.rd.spring2019.petproject.servlets.iDForChecking.impl.IDForCheckingImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.spring2019.petproject.servlets.viewsPathForServlets.ViewsPathForServlets.*;

public class ModifyOrderServlet extends HttpServlet {

    private IDForChecking idForChecking = new IDForCheckingImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher(MODIFY_ORDER_JSP);
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String ordersID = req.getParameter("ordersID");
        String clientsID = req.getParameter("clientsID");
        String productsID = req.getParameter("productsID");

        // check out if data from a browser are "empty"
        if (!ordersID.isEmpty() && !clientsID.isEmpty() && !productsID.isEmpty()) {
            OrderDBDao orderDBDao = new OrderDBDao();
            OrderService orderService = new OrderServiceImpl(orderDBDao);

            Client clientForModify = orderDBDao.findClient(Long.parseLong(clientsID));
            List<Car> carListForModify = new ArrayList<>();
            Car carForList = orderDBDao.findProduct(Long.parseLong(productsID));
            carListForModify.add(carForList);

            // prepare for checking client's and product's ID into BD
            List<Long> clientIDList = idForChecking.getClientIDForChecking();
            List<Long> productIDList = idForChecking.getProductIDForChecking();

            // check client's and product's ID into BD
            if ((clientIDList.contains(Long.parseLong(clientsID)))
                    && (productIDList.contains(Long.parseLong(productsID)))) {

                // modify an existing order through data from the site
                orderService.modifyOrder(Long.parseLong(ordersID), clientForModify, carListForModify);

                Order tempOrder = orderDBDao.findOrder(Long.parseLong(ordersID));
                req.setAttribute("order", tempOrder);
                doGet(req, resp);
            } else {
                doGet(req, resp);
            }
        } else {
            doGet(req, resp);
        }
    }

}
