package com.epam.rd.spring2019.petproject;

import com.epam.rd.spring2019.petproject.dao.impl.*;
import com.epam.rd.spring2019.petproject.services.ClientService;
import com.epam.rd.spring2019.petproject.services.OrderService;
import com.epam.rd.spring2019.petproject.services.CarService;
import com.epam.rd.spring2019.petproject.services.impl.ClientServiceImpl;
import com.epam.rd.spring2019.petproject.services.impl.OrderServiceImpl;
import com.epam.rd.spring2019.petproject.services.impl.CarServiceImpl;
import com.epam.rd.spring2019.petproject.validators.ValidationService;
import com.epam.rd.spring2019.petproject.validators.impl.ValidationServiceImpl;
import com.epam.rd.spring2019.petproject.view.AdminMenu;
import com.epam.rd.spring2019.petproject.view.ClientMenu;
import com.epam.rd.spring2019.petproject.view.MainMenu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

    // a start point of the entire application
    public static void main(String[] args) throws IOException {

        // configured all necessary dependencies for this application
        // realizing of the dependency injection
        ClientDBDao clientDBDao = new ClientDBDao();
        CarDBDao productDBDao = new CarDBDao();
        OrderDBDao orderDBDao = new OrderDBDao();
        ValidationService validationService = new ValidationServiceImpl();
        ClientService clientService = new ClientServiceImpl(clientDBDao, validationService);
        CarService carService = new CarServiceImpl(productDBDao);
        OrderService orderService = new OrderServiceImpl(orderDBDao);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        AdminMenu adminMenu = new AdminMenu(br, clientService, carService, orderService);
        ClientMenu clientMenu = new ClientMenu(br, clientService, carService, orderService);

        MainMenu menu = new MainMenu(br, adminMenu, clientMenu);
        menu.showMenu();
    }

}
