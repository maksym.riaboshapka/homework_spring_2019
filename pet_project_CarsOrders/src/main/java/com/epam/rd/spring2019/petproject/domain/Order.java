package com.epam.rd.spring2019.petproject.domain;

import java.util.List;
import java.util.Objects;

public class Order {

    private long id;
    private Client client;
    private List<Car> cars;

    public Order() {
    }

    public Order(Client client, List<Car> cars) {
        this.client = client;
        this.cars = cars;
    }

    public Order(long id, Client client, List<Car> cars) {
        this.id = id;
        this.client = client;
        this.cars = cars;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", client=" + client +
                ", product=" + cars +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id &&
                Objects.equals(client, order.client) &&
                Objects.equals(cars, order.cars);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, client, cars);
    }
}
