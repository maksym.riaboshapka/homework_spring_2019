package com.epam.rd.spring2019.petproject.servlets.carsServlets;

import com.epam.rd.spring2019.petproject.dao.impl.CarDBDao;
import com.epam.rd.spring2019.petproject.domain.Car;
import com.epam.rd.spring2019.petproject.services.CarService;
import com.epam.rd.spring2019.petproject.services.impl.CarServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.rd.spring2019.petproject.servlets.viewsPathForServlets.ViewsPathForServlets.*;

public class CarListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CarDBDao carDBDao = new CarDBDao();
        CarService carService = new CarServiceImpl(carDBDao);
        List<Car> carsList = carService.getAllCars();
        req.setAttribute("productsList", carsList);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher(CARS_LIST_JSP);
        requestDispatcher.forward(req, resp);
    }

}
