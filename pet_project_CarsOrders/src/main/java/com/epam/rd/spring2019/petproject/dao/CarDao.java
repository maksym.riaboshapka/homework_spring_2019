package com.epam.rd.spring2019.petproject.dao;

import com.epam.rd.spring2019.petproject.domain.Car;
import com.epam.rd.spring2019.petproject.domain.Car;

import java.util.List;

public interface CarDao {

    /**
     * save car
     *
     * @param car for save
     * @return true if client is saved
     */
    boolean saveCar(Car car);


    /**
     * to read a list of all products
     *
     * @return list of all products
     */
    List<Car> getAllCars();


    /**
     * modify car
     *
     * @param id      of car for save
     * @param car of car for save
     * @return true if car is modified
     */
    boolean modifyCar(long id, Car car);


    /**
     * deleteCar product by Id
     *
     * @param id of product
     * @return true if product was deleted
     */
    boolean deleteCar(long id);
}
