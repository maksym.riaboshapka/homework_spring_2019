package com.epam.rd.spring2019.petproject.services;

import com.epam.rd.spring2019.petproject.domain.Client;
import com.epam.rd.spring2019.petproject.domain.Order;
import com.epam.rd.spring2019.petproject.domain.Car;

import java.util.List;

public interface OrderService {

    /**
     * create order
     *
     * @param client   for order
     * @param cars list of cars for order
     */
    void createOrder(Client client, List<Car> cars);


    /**
     * get all orders
     *
     * @return list of all orders
     */
    List<Order> getAllOrders();

    /**
     * a method modify information about order
     *
     * @param id                         of order
     * @param clientForModifyOrder       of order
     * @param productsListForModifyOrder list of products for order
     */
    void modifyOrder(long id, Client clientForModifyOrder, List<Car> productsListForModifyOrder);

    /**
     * deleteCar order by id
     *
     * @param id of order
     */
    void delete(long id);
}
