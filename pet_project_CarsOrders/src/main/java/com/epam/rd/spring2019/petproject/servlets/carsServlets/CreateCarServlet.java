package com.epam.rd.spring2019.petproject.servlets.carsServlets;

import com.epam.rd.spring2019.petproject.dao.impl.CarDBDao;
import com.epam.rd.spring2019.petproject.domain.Car;
import com.epam.rd.spring2019.petproject.services.CarService;
import com.epam.rd.spring2019.petproject.services.impl.CarServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import static com.epam.rd.spring2019.petproject.servlets.viewsPathForServlets.ViewsPathForServlets.*;

public class CreateCarServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher(CREATE_CAR_JSP);
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String carsName = req.getParameter("name");
        String carsPrice = req.getParameter("price");

        //check out if data from a browser are "empty"
        if (!carsName.isEmpty() &&
                !carsPrice.isEmpty()) {
            CarDBDao clientDBDao = new CarDBDao();
            CarService carService = new CarServiceImpl(clientDBDao);

            // create a product through data from site
            carService.createCar(carsName, BigDecimal.valueOf(Long.parseLong(carsPrice)));

            // get the product for request argument of setAttribute()
            List<Car> carsList = carService.getAllCars();
            Car tempCar = carsList.get(carsList.size() - 1);
            String tempCarName = tempCar.getName();
            BigDecimal tempCarPrice = tempCar.getPrice();
            if ((tempCarName.equals(carsName) &&
                    (tempCarPrice.equals(BigDecimal.valueOf(Long.parseLong(carsPrice)))))) {
                Car carForRequest = new Car(tempCarName, tempCarPrice);
                req.setAttribute("product", carForRequest);
            }
            doGet(req, resp);
        } else {
            doGet(req, resp);
        }
    }


}
