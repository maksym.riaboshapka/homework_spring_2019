package com.epam.rd.spring2019.petproject.services.impl;

import com.epam.rd.spring2019.petproject.dao.impl.CarDBDao;
import com.epam.rd.spring2019.petproject.domain.Car;
import com.epam.rd.spring2019.petproject.services.CarService;

import java.math.BigDecimal;
import java.util.List;

public class CarServiceImpl implements CarService {

    private CarDBDao carDBDao;

    public CarServiceImpl(CarDBDao carDBDao) {
        this.carDBDao = carDBDao;
    }

    @Override
    public void createCar(String carName, BigDecimal price) {
        Car car = new Car(carName, price);
        boolean result = carDBDao.saveCar(car);
        if (result) {
            System.out.println("Car Saved: " + car);
        }
    }

    @Override
    public void modifyCar(long id, String carName, BigDecimal carPrice) {
        for (Car car : getAllCars()) {
            long carId = car.getId();
            if (carId == id) {
                car.setName(carName);
                car.setPrice(carPrice);
                boolean result = carDBDao.modifyCar(id, car);
                if (result) {
                    System.out.println("Car Saved: " + car);
                }
            }
        }
    }

    @Override
    public List<Car> getAllCars() {
        return carDBDao.getAllCars();
    }

    @Override
    public void deleteCar(long id) {
        for (Car car : getAllCars()) {
            long carId = car.getId();
            if (carId == id) {
                boolean result = carDBDao.deleteCar(id);
                if (result) {
                    System.out.println("Car Deleted: " + car);
                }
            }
        }
    }
}
