package com.epam.rd.spring2019.petproject.servlets.clientsServlets;

import com.epam.rd.spring2019.petproject.dao.impl.ClientDBDao;
import com.epam.rd.spring2019.petproject.domain.Client;
import com.epam.rd.spring2019.petproject.services.ClientService;
import com.epam.rd.spring2019.petproject.services.impl.ClientServiceImpl;
import com.epam.rd.spring2019.petproject.validators.ValidationService;
import com.epam.rd.spring2019.petproject.validators.impl.ValidationServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.rd.spring2019.petproject.servlets.viewsPathForServlets.ViewsPathForServlets.*;

public class DeleteClientServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher(DELETE_CLIENT_JSP);
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String clientID = req.getParameter("id");

        // check out if data from a browser are "empty"
        if (!clientID.isEmpty()) {
            ClientDBDao clientDBDao = new ClientDBDao();
            ValidationService validationService = new ValidationServiceImpl();
            ClientService clientService = new ClientServiceImpl(clientDBDao, validationService);
            Client clientForDelete = clientDBDao.findClient(Long.parseLong(clientID));
            // deleteCar an existing client
            clientService.deleteClient(Long.parseLong(clientID));
            req.setAttribute("client", clientForDelete);
            doGet(req, resp);
        } else {
            doGet(req, resp);
        }
    }
}
