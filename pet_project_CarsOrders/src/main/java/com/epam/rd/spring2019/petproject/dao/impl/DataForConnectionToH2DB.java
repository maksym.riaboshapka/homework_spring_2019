package com.epam.rd.spring2019.petproject.dao.impl;

public class DataForConnectionToH2DB {

    public static final String DB_URL = "jdbc:h2:tcp://localhost/~/OrderCar";
    public static final String LOGIN = "test";
    public static final String PASSWORD = "test";

}
