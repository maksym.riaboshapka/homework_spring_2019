package com.epam.rd.spring2019.petproject.servlets.ordersServlets;

import com.epam.rd.spring2019.petproject.dao.impl.OrderDBDao;
import com.epam.rd.spring2019.petproject.domain.Order;
import com.epam.rd.spring2019.petproject.services.OrderService;
import com.epam.rd.spring2019.petproject.services.impl.OrderServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.rd.spring2019.petproject.servlets.viewsPathForServlets.ViewsPathForServlets.*;

public class OrdersListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        OrderDBDao orderDBDao = new OrderDBDao();
        OrderService orderService = new OrderServiceImpl(orderDBDao);
        List<Order> ordersList = orderService.getAllOrders();
        req.setAttribute("ordersList", ordersList);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher(ORDERS_LIST_JSP);
        requestDispatcher.forward(req, resp);
    }


}
