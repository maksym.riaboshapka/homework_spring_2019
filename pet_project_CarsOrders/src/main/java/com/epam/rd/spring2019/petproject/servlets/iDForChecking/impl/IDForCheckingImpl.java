package com.epam.rd.spring2019.petproject.servlets.iDForChecking.impl;

import com.epam.rd.spring2019.petproject.dao.impl.ClientDBDao;
import com.epam.rd.spring2019.petproject.dao.impl.OrderDBDao;
import com.epam.rd.spring2019.petproject.dao.impl.CarDBDao;
import com.epam.rd.spring2019.petproject.domain.Car;
import com.epam.rd.spring2019.petproject.domain.Client;
import com.epam.rd.spring2019.petproject.domain.Order;
import com.epam.rd.spring2019.petproject.servlets.iDForChecking.IDForChecking;

import java.util.ArrayList;
import java.util.List;

public class IDForCheckingImpl implements IDForChecking {

    // prepare for checking order's ID into BD
    @Override
    public List<Long> getOrderIDForChecking() {
        OrderDBDao orderDBDao = new OrderDBDao();
        List<Order> orderList = orderDBDao.getAllOrders();
        List<Long> orderIDList = new ArrayList<>();
        for (Order order : orderList) {
            orderIDList.add(order.getId());
        }
        return orderIDList;
    }

    // prepare for checking client's ID into BD
    @Override
    public List<Long> getClientIDForChecking() {
        ClientDBDao clientDBDao = new ClientDBDao();
        List<Client> clientList = clientDBDao.getAllClients();
        List<Long> clientIDList = new ArrayList<>();
        for (Client client : clientList) {
            clientIDList.add(client.getId());
        }
        return clientIDList;
    }

    // prepare for checking product's ID into BD
    @Override
    public List<Long> getProductIDForChecking() {
        CarDBDao productDBDao = new CarDBDao();
        List<Car> carList = productDBDao.getAllCars();
        List<Long> productIDList = new ArrayList<>();
        for (Car car : carList) {
            productIDList.add(car.getId());
        }
        return productIDList;
    }

}
