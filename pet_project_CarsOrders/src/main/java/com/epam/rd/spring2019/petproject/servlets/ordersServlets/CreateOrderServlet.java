package com.epam.rd.spring2019.petproject.servlets.ordersServlets;

import com.epam.rd.spring2019.petproject.dao.impl.OrderDBDao;
import com.epam.rd.spring2019.petproject.domain.Client;
import com.epam.rd.spring2019.petproject.domain.Order;
import com.epam.rd.spring2019.petproject.domain.Car;
import com.epam.rd.spring2019.petproject.services.OrderService;
import com.epam.rd.spring2019.petproject.services.impl.OrderServiceImpl;
import com.epam.rd.spring2019.petproject.servlets.iDForChecking.IDForChecking;
import com.epam.rd.spring2019.petproject.servlets.iDForChecking.impl.IDForCheckingImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.spring2019.petproject.servlets.viewsPathForServlets.ViewsPathForServlets.*;

public class CreateOrderServlet extends HttpServlet {

    private IDForChecking idForChecking = new IDForCheckingImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher(CREATE_ORDER_JSP);
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String clientsID = req.getParameter("clientsID");
        String productsID = req.getParameter("productsID");

        //check out if data from a browser are "empty"
        if (!clientsID.isEmpty() && !productsID.isEmpty()) {

            OrderDBDao orderDBDao = new OrderDBDao();
            OrderService orderService = new OrderServiceImpl(orderDBDao);
            Client clientForCreateOrder = orderDBDao.findClient(Long.parseLong(clientsID));
            List<Car> listCarForCreateOrder = new ArrayList<>();
            Car carForCreateOrder = orderDBDao.findProduct(Long.parseLong(productsID));
            listCarForCreateOrder.add(carForCreateOrder);

            // prepare for checking client's and product's ID into BD
            List<Long> clientIDList = idForChecking.getClientIDForChecking();
            List<Long> productIDList = idForChecking.getProductIDForChecking();

            // check client's and product's ID into BD
            if ((clientIDList.contains(Long.parseLong(clientsID)))
                    && (productIDList.contains(Long.parseLong(productsID)))) {

                // create an order through data from site if client's and product's id are existed
                orderService.createOrder(clientForCreateOrder, listCarForCreateOrder);

                // get the order for request argument of setAttribute()
                List<Order> ordersList = orderService.getAllOrders();
                Order tempOrder = ordersList.get(ordersList.size() - 1);
                Client clientForChecking = tempOrder.getClient();
                String tempClientsID = String.valueOf(clientForChecking.getId());
                List<Car> listOfCarForChecking = tempOrder.getCars();
                Car carForChecking = listOfCarForChecking.get(listOfCarForChecking.size() - 1);
                String tempProductsID = String.valueOf(carForChecking.getId());
                Client clientForRequest = orderDBDao.findClient(Long.parseLong(tempClientsID));
                Car productForCarListForRequest = orderDBDao.findProduct(Long.parseLong(tempProductsID));
                List<Car> carListForRequest = new ArrayList<>();
                carListForRequest.add(productForCarListForRequest);
                if (tempClientsID.equals(clientsID) && tempProductsID.equals(productsID)) {
                    Order orderForRequest = new Order(clientForRequest, carListForRequest);
                    req.setAttribute("order", orderForRequest);
                }
            }
            doGet(req, resp);
        } else {
            doGet(req, resp);
        }

    }

}
