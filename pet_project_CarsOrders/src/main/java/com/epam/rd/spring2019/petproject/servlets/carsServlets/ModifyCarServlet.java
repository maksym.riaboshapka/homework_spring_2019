package com.epam.rd.spring2019.petproject.servlets.carsServlets;

import com.epam.rd.spring2019.petproject.dao.impl.CarDBDao;
import com.epam.rd.spring2019.petproject.domain.Car;
import com.epam.rd.spring2019.petproject.services.CarService;
import com.epam.rd.spring2019.petproject.services.impl.CarServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

import static com.epam.rd.spring2019.petproject.servlets.viewsPathForServlets.ViewsPathForServlets.*;

public class ModifyCarServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher(MODIFY_CAR_JSP);
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String carsID = req.getParameter("id");
        String carsName = req.getParameter("name");
        String carsPrice = req.getParameter("price");

        // check out if data from a browser are "empty"
        if (!carsID.isEmpty() &&
                !carsName.isEmpty() &&
                !carsPrice.isEmpty()) {
            CarDBDao carDBDao = new CarDBDao();
            CarService carService = new CarServiceImpl(carDBDao);

            // modify an existing product through data from the site
            carService.modifyCar(Long.parseLong(carsID),
                    carsName,
                    BigDecimal.valueOf(Long.parseLong(carsPrice)));

            // get the product for request argument of setAttribute()
            Car tempCar = carDBDao.findCar(Long.parseLong(carsID));
            req.setAttribute("product", tempCar);
            doGet(req, resp);
        } else {
            doGet(req, resp);
        }

    }

}
