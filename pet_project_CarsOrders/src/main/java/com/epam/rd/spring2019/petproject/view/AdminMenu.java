package com.epam.rd.spring2019.petproject.view;

import com.epam.rd.spring2019.petproject.domain.Car;
import com.epam.rd.spring2019.petproject.domain.Client;
import com.epam.rd.spring2019.petproject.domain.Order;
import com.epam.rd.spring2019.petproject.services.ClientService;
import com.epam.rd.spring2019.petproject.services.OrderService;
import com.epam.rd.spring2019.petproject.services.CarService;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AdminMenu {

    private final BufferedReader br;
    private final ClientService clientService;
    private final CarService carService;
    private final OrderService orderService;

    public AdminMenu(BufferedReader br, ClientService clientService,
                     CarService carService, OrderService orderService) {
        this.br = br;
        this.clientService = clientService;
        this.carService = carService;
        this.orderService = orderService;
    }

    public void show() throws IOException {


        while (true) {
            showMenu();
            switch (br.readLine()) {
                case "1":
                    createClient();
                    break;
                case "2":
                    modifyClient();
                    break;
                case "3":
                    deleteClient();
                    break;
                case "4":
                    System.out.println("All clients:");
                    showAllClients();
                    break;
                case "5":
                    createProduct();
                    break;
                case "6":
                    modifyProduct();
                    break;
                case "7":
                    deleteProduct();
                    break;
                case "8":
                    System.out.println("All products:");
                    showAllProducts();
                    break;
                case "9":
                    modifyOrder();
                    break;
                case "10":
                    deleteOrder();
                case "11":
                    showAllOrders();
                    break;
                case "R":
                    return;
                case "E":
                    return;
                default:
                    System.out.println("wrong input!!!");
            }
        }

    }

    private void showMenu() {
        System.out.println("1. Add client");
        System.out.println("2. Modify client");
        System.out.println("3. Remove client");
        System.out.println("4. List all clients");
        System.out.println();

        System.out.println("5. Add product");
        System.out.println("6. Modify product");
        System.out.println("7. Remove product");
        System.out.println("8. List all product");
        System.out.println();
        // everything does by id for the orders for the admin
        System.out.println("9. Modify order");
        System.out.println("10. Remove order");
        System.out.println("11. List all order");

        System.out.println("R. Return");
        System.out.println("E. Exit");
    }

    private void createClient() throws IOException {
        System.out.println("Input name: ");
        String name = br.readLine();
        System.out.println("Input surname: ");
        String surname = br.readLine();
        System.out.println("Input age:");
        int age = readInteger();
        System.out.println("Input phone number: ");
        String phoneNumber = br.readLine();
        System.out.println("Input email");
        String email = br.readLine();
        clientService.createClient(name, surname, age, phoneNumber, email);
    }


    private void modifyClient() throws IOException {
        showAllClients();
        System.out.println("Input client's ID for modify: ");
        long id = readLongId();
        for (Client client : clientService.getAllClients()) {
            long tempId = client.getId();
            if (tempId == id) {
                System.out.println("Input name: ");
                String name = br.readLine();
                System.out.println("Input surname: ");
                String surname = br.readLine();
                System.out.println("Input age:");
                int age = readInteger();
                System.out.println("Input phone number: ");
                String phoneNumber = br.readLine();
                System.out.println("Input email");
                String email = br.readLine();
                clientService.modifyClient(id, name, surname, age, phoneNumber, email);
                return;
            }
        }
    }

    private void deleteClient() {
        showAllClients();
        System.out.println("Input client's ID for remove: ");
        long id = readLongId();
        for (Client client : clientService.getAllClients()) {
            long tempId = client.getId();
            if (tempId == id) {
                clientService.deleteClient(id);
                return;
            }
        }
    }

    private void showAllClients() {
        for (Client client : clientService.getAllClients()) {
            System.out.println(client);
        }
    }

    private void createProduct() throws IOException {
        System.out.println("Input product's name: ");
        String productName = br.readLine();
        System.out.println("Input product's price:");
        BigDecimal productPrice = readBigDecimal();
        carService.createCar(productName, productPrice);
    }

    private void modifyProduct() throws IOException {
        showAllProducts();
        System.out.println("Input product's ID for modify: ");
        long id = readLongId();
        for (Car car : carService.getAllCars()) {
            long tempId = car.getId();
            if (tempId == id) {
                System.out.println("Input name: ");
                String productName = br.readLine();
                System.out.println("Input car's price:");
                BigDecimal productPrice = readBigDecimal();
                carService.modifyCar(id, productName, productPrice);
                return;
            }
        }
    }

    private void showAllProducts() {
        for (Car car : carService.getAllCars()) {
            System.out.println(car);
        }
    }

    private void deleteProduct() {
        showAllProducts();
        System.out.println("Input product's ID for remove: ");
        long id = readLongId();
        for (Car car : carService.getAllCars()) {
            long tempId = car.getId();
            if (tempId == id) {
                carService.deleteCar(id);
                return;
            } else {
                System.out.println("Choose correct Id of car for deleting");
            }
        }
    }


    private void modifyOrder() {
        showAllOrders();
        System.out.println("Input order's ID for modify:");
        long id = readLongId();
        for (Order order : orderService.getAllOrders()) {
            long tempId = order.getId();
            if (tempId == id) {
                Client clientForModifyOrder = getClientForModifyOrder();
                List<Car> productsListForModifyOrder = getProductsListForModifyOrder();
                orderService.modifyOrder(id, clientForModifyOrder, productsListForModifyOrder);
                return;
            }
        }
    }

    private Client getClientForModifyOrder() {
        Client clientForModifiedOrder = null;
        showAllClients();
        System.out.println("Enter client's ID for modify:");
        long clientID = readLongId();
        for (Client client : clientService.getAllClients()) {
            long clientTempId = client.getId();
            if (clientTempId == clientID) {
                clientForModifiedOrder = client;
            }
        }
        return clientForModifiedOrder;
    }

    private List<Car> getProductsListForModifyOrder() {
        showAllProducts();
        List<Car> listCars = new ArrayList<>();
        long productId;
        boolean exitFromWhile = true;
        while (exitFromWhile) {
            productId = readLongId();
            if (productId != -1) {
                for (Car car : carService.getAllCars()) {
                    long tempProductId = car.getId();
                    if (tempProductId == productId) {
                        listCars.add(car);
                        return listCars;
                    }
                }
            } else {
                exitFromWhile = false;
            }
        }
        return listCars;
    }

    private void deleteOrder() {
        showAllOrders();
        System.out.println("Input order's ID for remove: ");
        long id = readLongId();
        for (Order order : orderService.getAllOrders()) {
            long tempId = order.getId();
            if (tempId == id) {
                orderService.delete(id);
                return;
            } else {
                System.out.println("Choose correct Id of order for deleting");
            }
        }
    }

    private void showAllOrders() {
        for (Order order : orderService.getAllOrders()) {
            System.out.println(order);
        }
    }

    private int readInteger() {
        try {
            return Integer.parseInt(br.readLine());
        } catch (IOException | NumberFormatException ex) {
            System.out.println("Input number please!!!");
            // recursive call
            return readInteger();
        }
    }

    private long readLongId() {
        try {
            return Long.parseLong(br.readLine());
        } catch (IOException | NumberFormatException ex) {
            System.out.println("Input number please!!!");
            // recursive call
            return readLongId();
        }
    }

    private BigDecimal readBigDecimal() {
        try {
            return BigDecimal.valueOf(Long.parseLong(br.readLine()));
        } catch (IOException | NumberFormatException ex) {
            System.out.println("Input number please!!!");
            // recursive call
            return readBigDecimal();
        }
    }
}
