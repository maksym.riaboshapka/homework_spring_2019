
> Now when the project is rebuilding you need to use the button "Skip Tests"
> in the "Maven" tab! This is due to the fact that in the
> "homework_12_HTTP_HTTPS protocols" for tests we have to start the container
> of servlets "Tomcat" because we use the "URL" for running the tests. It is only
> after running the "Tomcat" you can turn off the button "Skip Tests"
> the "Maven" tab and rebuild project via "mvn clean install"
> or "Lifecycle" -> clean -> install -> "Run Maven Build" (using "test").

postmanCollection:  https://www.getpostman.com/collections/94974cc82215d4163cf0

http://localhost:8080/