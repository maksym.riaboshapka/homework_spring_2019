package com.epam.rd.spring2019.httphttpsprotocols;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

public class EchoServlet extends HttpServlet {

    private String message;

    public void init() throws ServletException {
        message = "Hello World";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("This is " + this.getClass().getName()
                + ", using the GET method");

        enumerationHeaders(request, out);

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("This is " + this.getClass().getName()
                + ", using the POST method");

        enumerationHeaders(request, out);

    }

    public void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("This is " + this.getClass().getName()
                + ", using the PUT method");

        enumerationHeaders(request, out);

    }

    public void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("This is " + this.getClass().getName()
                + ", using the DELETE method");

        enumerationHeaders(request, out);

    }

    public void destroy() {
        super.destroy();
    }

    private void enumerationHeaders(HttpServletRequest request, PrintWriter out) {
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            Object headerName = headerNames.nextElement();
            String headerValue = request.getHeader((String) headerName);
            out.println(String.format("<h1>%s:%s</h1>", headerName, headerValue));
        }
    }

}
