package com.epam.rd.spring2019.httphttpsprotocols;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

public class EchoServletTest {

    private static final String host = "http://localhost:8080";
    private static final String service = "/";
    private static final String url = host + service;

    @Test
    public void doGetTest() throws IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response = httpclient.execute(httpGet);
        try {
            System.out.println(response.getStatusLine());
            System.out.println(Arrays.toString(response.getAllHeaders()));

            HttpEntity entity = response.getEntity();
            EntityUtils.consume(entity);
        } finally {
            response.close();
        }
    }

    @Test
    public void doPost() throws IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        CloseableHttpResponse response = httpclient.execute(httpPost);
        try {
            System.out.println(response.getStatusLine());
            System.out.println(Arrays.toString(response.getAllHeaders()));

            HttpEntity entity = response.getEntity();
            EntityUtils.consume(entity);
        } finally {
            response.close();
        }
    }

    @Test
    public void doPut() throws IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPut httpPut = new HttpPut(url);
        CloseableHttpResponse response = httpclient.execute(httpPut);
        try {
            System.out.println(response.getStatusLine());
            System.out.println(Arrays.toString(response.getAllHeaders()));

            HttpEntity entity = response.getEntity();
            EntityUtils.consume(entity);
        } finally {
            response.close();
        }
    }

    @Test
    public void doDelete() throws IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpDelete httpDelete = new HttpDelete(url);
        CloseableHttpResponse response = httpclient.execute(httpDelete);
        try {
            System.out.println(response.getStatusLine());
            System.out.println(Arrays.toString(response.getAllHeaders()));

            HttpEntity entity = response.getEntity();
            EntityUtils.consume(entity);
        } finally {
            response.close();
        }
    }
}