package com.epam.rd.spring2019.concurrent_multithreading.taskThree_Queue;

class Consumer extends Thread {
    private MyQueueWithLock queue;

    public Consumer(MyQueueWithLock queue) {
        this.queue = queue;
        this.setName("Consumer");
    }

    @Override
    public void run() {
        while (true) {
            queue.get();
            if (isInterrupted()) {
                return;
            }
        }
    }
}
