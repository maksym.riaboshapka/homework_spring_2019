package com.epam.rd.spring2019.concurrent_multithreading.taskOne_DeadLock;

public class AppDeadlock {
    public static void main(String[] args) throws Exception {
        Car audi = new Car("Audi_Q8");
        Car bmw = new Car("BMW_X7");

        new Thread(() -> audi.skipAnotherCar(bmw)).start();
        new Thread(() -> bmw.skipAnotherCar(audi)).start();
    }
}
