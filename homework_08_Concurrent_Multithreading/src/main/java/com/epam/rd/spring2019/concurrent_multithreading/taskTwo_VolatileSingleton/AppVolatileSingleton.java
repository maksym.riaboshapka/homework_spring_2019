package com.epam.rd.spring2019.concurrent_multithreading.taskTwo_VolatileSingleton;

import java.util.ArrayList;
import java.util.List;

public class AppVolatileSingleton {

    private static AppVolatileSingleton singletonInstance = null;
    private volatile static boolean isInstanceCreated = false;
    private volatile static List<Integer> listOfElementsForSingletonAsExample = new ArrayList<>();

    private AppVolatileSingleton() {
        listOfElementsForSingletonAsExample.add(1);
        listOfElementsForSingletonAsExample.add(2);
        listOfElementsForSingletonAsExample.add(3);
        listOfElementsForSingletonAsExample.add(4);
        listOfElementsForSingletonAsExample.add(5);
        listOfElementsForSingletonAsExample.add(6);
        listOfElementsForSingletonAsExample.add(7);
        listOfElementsForSingletonAsExample.add(8);
        listOfElementsForSingletonAsExample.add(9);
    }

    public static void main(String[] args) {
        createThread();
        createThread();
    }

    private static void createThread() {
        Thread quickThread = new Thread(() -> {
            System.out.println("Thread " + Thread.currentThread().getName() + " is running now!");
            System.out.println(AppVolatileSingleton.getSingletonInstance().getClass());
            for (Integer element : listOfElementsForSingletonAsExample) {
                System.out.println(element);
            }
            System.out.println("Good bye from thread " + Thread.currentThread().getName());
        });
        quickThread.start();
    }

    private static synchronized AppVolatileSingleton getSingletonInstance() {
        if (!isInstanceCreated) {
            try {
                if (!isInstanceCreated) {
                    singletonInstance = new AppVolatileSingleton();
                    isInstanceCreated = true;
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        return singletonInstance;
    }

}
