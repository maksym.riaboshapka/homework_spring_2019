package com.epam.rd.spring2019.concurrent_multithreading.taskThree_Queue;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class MyQueueWithLock {
    private int value;
    private boolean valueSet = false;
    private ReentrantLock locker;
    private Condition condition;

    MyQueueWithLock(ReentrantLock lock) {
        locker = lock;
        condition = locker.newCondition();
    }

    public void put(int value) {
        locker.lock();
        while (valueSet) {
            try {
                condition.await();
            } catch (InterruptedException e) {
                System.out.println("put interrupted was handled");
            }
        }

        this.value = value;
        valueSet = true;
        System.out.println("Sent: " + value);
        condition.signal();
        locker.unlock();
    }


    public int get() {
        locker.lock();
        while (!valueSet) {
            try {
                condition.await();
            } catch (InterruptedException e) {
                System.out.println("get interrupted was handled");
                Thread.currentThread().interrupt();
                return 0;
            }
        }
        System.out.println("Received: " + value);
        valueSet = false;
        condition.signal();
        locker.unlock();
        return value;
    }
}
