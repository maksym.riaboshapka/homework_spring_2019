package com.epam.rd.spring2019.concurrent_multithreading.taskOne_DeadLock;

public class Car {
    private final String name;

    public Car(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public synchronized void skipAnotherCar(Car trafficCar) {
        System.out.format("%s: %s" + "  has skiped to me!%n", this.name, trafficCar.getName());
        trafficCar.skipAnotherCarBack(this);
    }

    public synchronized void skipAnotherCarBack(Car trafficCar) {
        System.out.format("%s: %s"
                        + " has skiped back to me!%n",
                this.name, trafficCar.getName());
    }
}
