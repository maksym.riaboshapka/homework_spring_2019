package com.epam.rd.spring2019.servlets.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class UserDAO {

    static Connection currentCon = null;
    static ResultSet rs = null;

    public static UserBean login(UserBean bean) {
        Statement stmt = null;

        String userName = bean.getUserName();
        String password = bean.getPassword();

        String searchQuery =
                "select * from users where username='"
                        + userName
                        + "' AND password='"
                        + password
                        + "'";

        System.out.println("Your user name is " + userName);
        System.out.println("Your password is " + password);
        System.out.println("Query: " + searchQuery);

        try {
            currentCon = DBConnectionManager.getConnection();
            stmt = currentCon.createStatement();
            rs = stmt.executeQuery(searchQuery);
            boolean more = rs.next();

            if (!more) {
                System.out.println("Sorry, you are not a registered user!");
                bean.setValid(false);
            } else if (more) {
                String firstName = rs.getString("FirstName");
                String lastName = rs.getString("LastName");

                System.out.println("Welcome " + firstName);
                bean.setFirstName(firstName);
                bean.setLastName(lastName);
                bean.setValid(true);
            }
        } catch (Exception ex) {
            System.out.println("Log In failed: An Exception has occurred! " + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    System.out.println(ex);
                }
                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    System.out.println(ex);
                }
                stmt = null;
            }

            if (currentCon != null) {
                try {
                    currentCon.close();
                } catch (Exception ex) {
                    System.out.println(ex);
                }

                currentCon = null;
            }
        }

        return bean;
    }

}