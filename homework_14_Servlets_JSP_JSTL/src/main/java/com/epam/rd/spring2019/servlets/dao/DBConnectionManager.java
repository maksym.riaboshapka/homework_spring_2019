package com.epam.rd.spring2019.servlets.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionManager {

    static Connection con;
    static String url;

    public static Connection getConnection() {
        try {
            // assuming "users" is your DataSource name
            url = "jdbc:odbc:" + "users";

            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");

            try {
                // "user" with your SQL Server username, "password" with your SQL Server password
                con = DriverManager.getConnection(url, "root", "root");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        }

        return con;
    }

}