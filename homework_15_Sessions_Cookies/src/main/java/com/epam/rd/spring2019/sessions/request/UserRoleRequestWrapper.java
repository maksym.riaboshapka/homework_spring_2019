package com.epam.rd.spring2019.sessions.request;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.List;

public class UserRoleRequestWrapper extends HttpServletRequestWrapper {

    private String user;
    private List<String> roles = null;
    private HttpServletRequest realRequest;

    public UserRoleRequestWrapper(String user,
                                  List<String> roles,
                                  HttpServletRequest request) {
        super(request);
        this.user = user;
        this.roles = roles;
        this.realRequest = request;
    }

    @Override
    public boolean isUserInRole(String role) {
        if (roles == null) {
            return this.realRequest.isUserInRole(role);
        }
        return roles.contains(role);
    }
}