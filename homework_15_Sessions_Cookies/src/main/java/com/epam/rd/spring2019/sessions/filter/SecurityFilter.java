package com.epam.rd.spring2019.sessions.filter;

import com.epam.rd.spring2019.sessions.bean.UserAccount;
import com.epam.rd.spring2019.sessions.request.UserRoleRequestWrapper;
import com.epam.rd.spring2019.sessions.utils.AppUtils;
import com.epam.rd.spring2019.sessions.utils.SecurityUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebFilter("/*")
public class SecurityFilter implements Filter {

    public SecurityFilter() {
    }

    @Override
    public void init(FilterConfig fConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        String servletPath = request.getServletPath();

        UserAccount loginedUser = AppUtils.getLoginedUser(request.getSession());

        if (servletPath.equals("/login")) {
            chain.doFilter(request, response);
            return;
        }
        HttpServletRequest wrapRequest = request;

        if (loginedUser != null) {
            String userName = loginedUser.getUserName();

            List<String> roles = loginedUser.getRoles();

            wrapRequest = new UserRoleRequestWrapper(userName, roles, request);
        }

        if (SecurityUtils.isSecurityPage(request)) {

            if (loginedUser == null) {

                String requestUri = request.getRequestURI();

                int redirectId =
                        AppUtils.storeRedirectAfterLoginUrl(request.getSession(), requestUri);

                response.sendRedirect(wrapRequest.getContextPath()
                        + "/login?redirectId=" + redirectId);
                return;
            }

        }

        chain.doFilter(wrapRequest, response);
    }

    @Override
    public void destroy() {
    }

}