package com.epam.rd.spring2019.sessions.utils;

import com.epam.rd.spring2019.sessions.bean.UserAccount;
import com.epam.rd.spring2019.sessions.config.SecurityConfig;

import java.util.HashMap;
import java.util.Map;


public class DataDAO {

    private static final Map<String, UserAccount> mapUsers = new HashMap<String, UserAccount>();

    static {
        initUsers();
    }

    private static void initUsers() {

        UserAccount user =
                new UserAccount("user1",
                        "123",
                        SecurityConfig.ROLE_IS_SECURE);

        mapUsers.put(user.getUserName(), user);
    }

    public static UserAccount findUser(String userName, String password) {
        UserAccount usr = mapUsers.get(userName);
        if (usr != null && usr.getPassword().equals(password)) {
            return usr;
        }
        return null;
    }

}
