package com.epam.rd.spring2019.sessions.config;

import java.util.*;

public class SecurityConfig {

    public static final String ROLE_IS_SECURE = "SECURE";

    private static final Map<String, List<String>> mapConfig =
            new HashMap<String, List<String>>();

    static {
        init();
    }

    private static void init() {

        List<String> urlPatterns1 = new ArrayList<String>();

        urlPatterns1.add("/userInfo");

        mapConfig.put(ROLE_IS_SECURE, urlPatterns1);

    }

    public static Set<String> getAllAppRoles() {
        return mapConfig.keySet();
    }

    public static List<String> getUrlPatternsForRole(String role) {
        return mapConfig.get(role);
    }

}
