# homework_13_REST

This "homework_13_REST" illustrates the operation of the REST server for
the essence of "car's damages" from the pet project.

* 1.To build project execute in cmd: 
> mvn clean install

* 2.To create the database go to the next folder:
> ...\homework_spring_2019\homework_13_REST\>
*  and run the command:
> mvn liquibase:update
