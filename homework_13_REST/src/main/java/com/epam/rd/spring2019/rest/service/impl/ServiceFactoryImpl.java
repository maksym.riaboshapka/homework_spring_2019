package com.epam.rd.spring2019.rest.service.impl;

import com.epam.rd.spring2019.rest.entity.Damage;
import com.epam.rd.spring2019.rest.service.Service;
import com.epam.rd.spring2019.rest.service.ServiceFactory;

public class ServiceFactoryImpl implements ServiceFactory<Service> {

    private static final ServiceFactoryImpl serviceFactory = new ServiceFactoryImpl();

    private Service<Damage> damageService = new DamageServiceImpl();


    public static ServiceFactoryImpl getServiceFactory() {
        return serviceFactory;
    }

    @Override
    public DamageServiceImpl getDamageService() {
        return (DamageServiceImpl) this.damageService;
    }

}
