package com.epam.rd.spring2019.rest.service;

import com.epam.rd.spring2019.rest.service.impl.DamageServiceImpl;

public interface ServiceFactory<Service> {

    DamageServiceImpl getDamageService();

}
