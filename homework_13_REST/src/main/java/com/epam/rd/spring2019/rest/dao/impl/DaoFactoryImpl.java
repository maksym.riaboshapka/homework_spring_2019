package com.epam.rd.spring2019.rest.dao.impl;

import com.epam.rd.spring2019.rest.dao.Dao;
import com.epam.rd.spring2019.rest.dao.DaoFactory;
import com.epam.rd.spring2019.rest.entity.Damage;

public class DaoFactoryImpl implements DaoFactory {
    private static final DaoFactoryImpl daoFactory = new DaoFactoryImpl();

    private final Dao<Damage> damageDao = new DamageDaoImpl();

    public static DaoFactoryImpl getDaoFactoryImpl() {
        return daoFactory;
    }

    @Override
    public DamageDaoImpl getDamageDaoImpl() {
        return (DamageDaoImpl) this.damageDao;
    }

}
