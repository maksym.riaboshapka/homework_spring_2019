package com.epam.rd.spring2019.rest.dao;

import com.epam.rd.spring2019.rest.dao.impl.DamageDaoImpl;

public interface DaoFactory {

    DamageDaoImpl getDamageDaoImpl();

}
