package com.epam.rd.spring2019.rest.validator;

public interface Validator<E> {

    boolean validate(E entity);

}
