package com.epam.rd.spring2019.rest.service.impl;

import com.epam.rd.spring2019.rest.dao.impl.DaoFactoryImpl;
import com.epam.rd.spring2019.rest.entity.Damage;
import com.epam.rd.spring2019.rest.service.Service;

import java.util.List;

public class DamageServiceImpl implements Service<Damage> {

    @Override
    public boolean create(Damage element) {
        return DaoFactoryImpl.getDaoFactoryImpl().getDamageDaoImpl().create(element);
    }

    @Override
    public Damage read(int id) {
        return DaoFactoryImpl.getDaoFactoryImpl().getDamageDaoImpl().read(id);
    }

    @Override
    public List<Damage> read() {
        return DaoFactoryImpl.getDaoFactoryImpl().getDamageDaoImpl().read();
    }

    @Override
    public boolean update(Damage element) {
        return DaoFactoryImpl.getDaoFactoryImpl().getDamageDaoImpl().update(element);
    }

    @Override
    public boolean delete(int id) {
        return DaoFactoryImpl.getDaoFactoryImpl().getDamageDaoImpl().delete(id);
    }

}
