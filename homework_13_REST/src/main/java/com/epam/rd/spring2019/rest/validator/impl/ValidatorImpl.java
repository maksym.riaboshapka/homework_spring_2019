package com.epam.rd.spring2019.rest.validator.impl;

import com.epam.rd.spring2019.rest.entity.Damage;
import com.epam.rd.spring2019.rest.validator.Validator;

public class ValidatorImpl implements Validator<Damage> {

    @Override
    public boolean validate(Damage entity) {
        if (entity != null) {
            if (entity.getId() > 0 && entity.getSum() >= 0) return true;
        }
        return false;
    }
}
