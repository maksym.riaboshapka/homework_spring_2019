package com.epam.rd.spring2019.rest.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBManager {

    public static Connection getConnection(Properties properties) throws ClassNotFoundException {
        try {
            String url = properties.getProperty("url");
            String username = properties.getProperty("username");
            String password = properties.getProperty("password");
            Class.forName(properties.getProperty("driver"));
            return DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }

}
